import StateProviderSimulation from "./js/state_provider_simulation.js";
import ViewManagerDefault from "./js/view_manager_default.js";
import ViewAttitude from "./js/view_attitude.js";
import ViewTop from "./js/view_top.js";
import ViewMercator from "./js/view_mercator.js";
import { Permissions } from "./js/state_provider.js";
import StateProviderServer from "./js/state_provider_server.js";
import View from "./js/view.js";
import KeyListener, { KeyBinding, KeyContinuationMode } from "./js/key_listener.js";

var view_manager = new ViewManagerDefault("MAIN");
let view_attitude = document.getElementById("view-attitude") as ViewAttitude;
view_manager.registerView(view_attitude);
let view_top = document.getElementById("view-top") as ViewTop;
view_manager.registerView(view_top);
let view_mercator = document.getElementById("view-mercator") as ViewMercator;
view_manager.registerView(view_mercator);

(globalThis as any).view_manager = view_manager;
(globalThis as any).StateProviderSimulation = StateProviderSimulation;
(globalThis as any).StateProviderServer = StateProviderServer;
(globalThis as any).test_func_0 = () => {
  view_attitude.enabled = true;
  view_top.enabled = true;
  view_mercator.enabled = true;

  view_manager._state_manager.registerProvider((globalThis as any).provider);

  view_manager.start();
};
(globalThis as any).test_func_1 = () => {
  view_attitude.enabled = true;
  view_top.enabled = true;
  view_mercator.enabled = true;

  view_manager._state_manager.registerProvider((globalThis as any).provider);

  view_manager.start();
  setInterval(() => { view_manager._state_manager.state.current.aircraft_attitude.yaw += 1;}, 30);
};
(globalThis as any).test_func_2 = () => {
  view_attitude.enabled = true;
  view_top.enabled = true;
  view_mercator.enabled = true;

  view_manager._state_manager.registerProvider((globalThis as any).provider);

  view_manager.start();
  setInterval(() => { view_manager._state_manager.state.current.aircraft_attitude.yaw += 1;}, 30);
  setInterval(() => { view_manager._state_manager.state.current.camera_attitude.yaw += 1 / 2;}, 30);
};
(globalThis as any).test_func_3 = () => {
  view_attitude.enabled = true;
  view_top.enabled = true;
  view_mercator.enabled = true;

  view_manager._state_manager.registerProvider((globalThis as any).provider);

  view_manager.start();
  setInterval(() => { view_manager._state_manager.state.current.aircraft_attitude.yaw += 1;}, 30);
  setInterval(() => { view_manager._state_manager.state.current.camera_attitude.yaw += 1 / 2;}, 30);
  setInterval(() => { view_manager._state_manager.state.target.camera_attitude.yaw += 1 / 1.5;}, 30);
};
(globalThis as any).test_func_4 = () => {
  view_attitude.enabled = true;
  view_top.enabled = true;
  view_mercator.enabled = true;

  (globalThis as any).provider = new StateProviderServer("AIRCRAFT");

  view_manager._state_manager.registerProvider((globalThis as any).provider);

  view_manager.start();
  
  (globalThis as any).provider.connect("ws://localhost:8080/websocket");
};
(globalThis as any).test_func_5 = () => {
  (globalThis as any).provider.websocket.send('{"permissions":[{"level":1},{"level":1},{"level":1},{"level":1}]}')
};
(globalThis as any).test_func_6 = () => {
  let key_listener = new KeyListener();
  key_listener.addKeyBinding(new KeyBinding(["+"], () => {view_manager.getStateManager().setCameraZoom(view_manager._state_manager.state.target.camera_zoom + 0.1)}, "Move target camera attitude up", KeyContinuationMode.RESTART));
  key_listener.addKeyBinding(new KeyBinding(["-"], () => {view_manager.getStateManager().setCameraZoom(view_manager._state_manager.state.target.camera_zoom - 0.1)}, "Move target camera attitude up", KeyContinuationMode.RESTART));
  key_listener.addKeyBinding(new KeyBinding(["arrowdown"], () => {}, "Move target camera attitude down", KeyContinuationMode.RESTART));
  key_listener.addKeyBinding(new KeyBinding(["arrowleft"], () => {}, "Move target camera attitude left", KeyContinuationMode.RESTART));
  key_listener.addKeyBinding(new KeyBinding(["arrowright"], () => {}, "Move target camera attitude right", KeyContinuationMode.RESTART));
};


let a = new ViewAttitude();
let b = new ViewTop();
let c = new ViewMercator();

(globalThis as any).Permissions = Permissions;
(globalThis as any).KeyListener = KeyListener;
(globalThis as any).KeyBinding = KeyBinding;

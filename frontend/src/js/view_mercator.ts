import { color } from "./grafic_elements.js";
import { State } from "./state.js";
import { ViewMode } from "./utils.js";
import View, { checkViewState, drawOverlay, ViewState } from "./view.js";
import ViewManager from "./view_manager.js";

const ge_line_thickness = 1.75;

class ViewMercatorConfig {
}

class ViewMercatorCache {
}

export default class ViewMercator extends HTMLCanvasElement implements View {

  private _enabled: boolean = false;
  private _config: ViewMercatorConfig = new ViewMercatorConfig();
  private _cache: ViewMercatorCache = new ViewMercatorCache();

  private _view_manager: ViewManager | null = null;
  private _state: ViewState = ViewState.INIT;

  private _context: CanvasRenderingContext2D;

  private _needs_redraw: boolean = true;

  private _mouse_active: boolean = false;

  constructor() {
    super();

    // Get the 2d drawing context from the canvas.
    let context: CanvasRenderingContext2D | null = this.getContext("2d", { alpha: false });
    if (context === null) {
      throw new Error("context is null");
    }
    this._context = context;

    // Set default values for text drawing.
    this._context.textBaseline = "middle";
    this._context.textAlign = "center";
    this._context.font = '12px "Source Code Pro"';

    this.addEventListener("mousedown", (e) => {
      if (e.buttons == 1) {
        this._mouse_active = true;
        this.view_manager?.getStateManager().setCameraAttitudeYawPitch((e.clientX - this.offsetLeft) / this.width * 360, (e.clientY - this.offsetTop) / this.offsetHeight * 90);
      }
    });

    this.addEventListener("mouseup", (e) => {
      this._mouse_active = false;
    });

    this.addEventListener("mouseleave", (e) => {
      this._mouse_active = false;
    });

    this.addEventListener("mousemove", (e) => {
      if (this._mouse_active) {
        this.view_manager?.getStateManager().setCameraAttitudeYawPitch((e.clientX - this.offsetLeft) / this.width * 360, (e.clientY - this.offsetTop) / this.offsetHeight * 90);
      }
    });

    // Draw the first frame onto the canvas.
    this.resize();
    this.draw(0);
  }

  public get enabled() : boolean {
    return this._enabled;
  }

  public set enabled(value : boolean) {
    this._enabled = value;

    this.draw(0);
  }

  public get state(): ViewState {
    return this._state;
  }

  public get view_manager(): ViewManager | null {
    return this._view_manager;
  }


  register(manager: ViewManager): void {
    this._view_manager = manager;

    this.handleStateChange(State.MANAGEMENT);
  }

  unregister(): void {
    this._view_manager = null;

    this.handleStateChange(State.MANAGEMENT);
  }

  resize(width?: number, height?: number): void {

    this.width = width || this.offsetWidth;
    this.height = height || this.offsetHeight;

    this._needs_redraw = true;

    // Precompute values which are used often in the next section.
    let width_2 = this.width / 2;
    let height_2 = this.height / 2;

    let size = Math.min(this.width, this.height);
    let size_2 = size / 2;
    

  }

  draw(timestamp: number, force_overlay: boolean = false, force_redraw: boolean = false): void {

    if (!this._enabled || this._state != ViewState.OK || force_overlay) {
      drawOverlay(this, this._context);
      return;
    }

    if (!this._needs_redraw && !force_redraw) {
      return;
    }

    // Make sure the state manager is available.
    let state_manager = this.view_manager?.getStateManager();
    if (!state_manager) {
      this._state = checkViewState(this);
      return;
    }

    

    this._context.textAlign = "center";
    this._context.textBaseline = "middle";

    // transform matrix; 0, 0 is now in the center
    this._context.setTransform();
    this._context.translate(this.width / 2, this.height / 2);
    let transform_matrix = this._context.getTransform();

    // ======== bottom layer ========

    // background
    this._context.fillStyle = color.black;
    this._context.fillRect(-this.width / 2, -this.height / 2, this.width, this.height);

    this._needs_redraw = false;
  }

  handleStateChange(type: State.ChangeType): void {

    if (type & State.MANAGEMENT) {
      this._state = checkViewState(this);
    }

    if (type & (State.STATE_CURRENT_CAMERA_ATTITUDE | State.STATE_CURRENT_CAMERA_ZOOM | State.STATE_CURRENT_CAMERA_FOCUS | State.STATE_TARGET_CAMERA_ATTITUDE | State.STATE_TARGET_CAMERA_ZOOM | State.STATE_TARGET_CAMERA_FOCUS)) {
      this._needs_redraw = true;
    }
  }
}

customElements.define("view-mercator", ViewMercator, { extends: "canvas" });

import StateManager from "./state_manager.js";
import View from "./view.js";

export default interface ViewManager {

  force_view_overlay: boolean;

  /**
   * Returns the name of the view manager.
   */
  getName(): string;

  /**
   * Register a view with the view manager.
   * @param view is the View to register.
   */
  registerView(view: View): void;

  /**
   * Unregister a view from the view manager.
   * @param view is the View to unregister.
   */
  unregisterView(view: View): void;

  /**
   * Start drawing all registered views.
   */
  start(): void;

  /**
   * Stop drawing all registered views.
   */
  start(): void;

  /**
   * Get the state manager from the view manager.
   */
  getStateManager(): StateManager;
}

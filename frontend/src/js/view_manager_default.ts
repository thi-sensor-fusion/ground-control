import { State } from "./state.js";
import StateManager from "./state_manager.js";
import StateManagerSingle from "./state_manager_single.js";
import View from "./view.js";
import ViewManager from "./view_manager.js";

interface IConstructor<T> {
  new (...args: any[]): T;
}


/**
 * ViewManagerDefault holds the StateManager for a aircraft and a list of views
 * to control the state.
 */
export default class ViewManagerDefault<TStateManager extends StateManager> implements ViewManager {

  private _name: string;
  /* private */ _state_manager: StateManager;
  /* private */ _views: View[] = [];
  private _resize_observer: ResizeObserver;
  private _animation_frame_request_id: number = 0;
  private _force_view_overlay: boolean = false;
  private _force_redraw: boolean = false;

  constructor(name: string) {
    this._name = name;
    this._state_manager = new StateManagerSingle(name, this.handleStateChange.bind(this));

    this._resize_observer = new ResizeObserver((entries) => {
      entries.forEach((entry) => {
        let view = entry.target as View;
        view.resize(entry.borderBoxSize[0].inlineSize, entry.borderBoxSize[0].blockSize);
        view.draw(0);
      });
    });
  }

  public get force_view_overlay(): boolean {
    return this._force_view_overlay
  }
  
  public set force_view_overlay(value : boolean) {
    this._force_view_overlay = value;
    this._force_redraw = true;
  }
  

  getName(): string {
    return this._name;
  }

  /**
   * Registers a view with the ViewManager.
   */
  registerView(view: View): void {
    view.register(this);
    this._views.push(view);
    this._resize_observer.observe(view);
  }

  /**
   * Unregisters a view from the ViewManager.
   */
  unregisterView(view: View): void {
    this._resize_observer.unobserve(view);
    view.unregister();
    view.draw(0, true);
    this._views.splice(this._views.indexOf(view));
  }

  /**
   * Start drawing all registered views.
   */
  start(): void {
    this._animation_frame_request_id = requestAnimationFrame(this.handleAnimationFrame.bind(this));
  }

  /**
   * Stop drawing all registered views.
   */
  stop(): void {
    if (this._animation_frame_request_id != 0) {
      cancelAnimationFrame(this._animation_frame_request_id);
    }
  }

  getStateManager(): StateManager {
    return this._state_manager;
  }

  /**
   * This method is called for every frame and is responsible for drawing
   * requesting each view to draw itself. Afterwards it requests to be called
   * for the next frame.
   */
  handleAnimationFrame(timestamp: DOMHighResTimeStamp): void {

    this._views.forEach((view) => {
      view.draw(timestamp, this.force_view_overlay, this._force_redraw);
    });

    this._force_redraw = false;

    // Request to be called on the next frame.
    this._animation_frame_request_id = requestAnimationFrame(this.handleAnimationFrame.bind(this));
  }

  /**
   * This method is a callback called by the StateManager to notify about a
   * state change.
   * @param type represents the kindes of state changes that occured.
   */
  handleStateChange(type: State.ChangeType): void {
    this._views.forEach((view) => {
      view.handleStateChange(type);
    });
  }
}

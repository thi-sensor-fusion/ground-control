import StateManager from "./state_manager.js";
import StateProvider, { Permissions } from "./state_provider.js";

export default class StateProviderSimulation implements StateProvider {

  private name: string;
  private state_manager: StateManager | null = null;

  constructor(name: string) {
    this.name = name;
  }

  getName(): string {
    return this.name;
  }

  register(state_manager: StateManager): void {
    this.state_manager = state_manager;
  }

  unregister(): void {
    this.state_manager = null;
  }

  changePermissions(permissions: Permissions): void {
    if (this.state_manager != null) {
      this.state_manager.handlePermissionsChange(permissions);
    }
  }

  applyChangedState(): void {
    if (this.state_manager != null) {
      this.state_manager.state.current.camera_attitude.yaw = this.state_manager.state.target.camera_attitude.yaw;
      this.state_manager.state.current.camera_attitude.pitch = this.state_manager.state.target.camera_attitude.pitch;
      this.state_manager.state.current.camera_zoom = this.state_manager.state.target.camera_zoom;
      this.state_manager.state.current.camera_focus = this.state_manager.state.target.camera_focus;
    }
  }
}

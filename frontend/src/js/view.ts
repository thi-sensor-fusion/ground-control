import { color } from "./grafic_elements.js";
import { State } from "./state.js";
import ViewManager from "./view_manager.js";

/**
 * View is the interface which all view types should implement.
 */
export default interface View extends HTMLCanvasElement {

  enabled: boolean;

  get state(): ViewState;

  get view_manager(): ViewManager | null;

  /**
   * Register the passed view manager in the view. The manager should be
   * used to access the state from the state manager.
   * @param manager is the manager of the view.
   */
  register(manager: ViewManager): void;

  /**
   * Unregister the registered view manager from the view.
   */
  unregister(): void;

  /**
   * Draws the next frame onto the canvas.
   * @param timestamp is the current timestamp in milliseconds.
   * @param force_overlay is a flag indicating whether the info overlay should
   * be drawn instead.
   */
  draw(timestamp: number, force_overlay?: boolean, force_redraw?: boolean): void;

  /**
   * This method is a callback called by the ViewManager to notify about a
   * state change from the StateManager.
   * @param type represents the kindes of state changes that occured.
   */
  handleStateChange(type: State.ChangeType): void;

  /**
   * Change the resolution of the view.
   * @param width specifies the new desired width. If it is ommited the
   * elements offsetWidth is used.
   * @param height specifies the new desired height. If it is ommited the
   * elements offsetHeight is used.
   */
  resize(width?: number, height?: number): void;
}

export enum ViewState {
  OK,
  INIT,
  NO_VIEW_MANAGER,
  NO_STATE_MANAGER,
  NO_STATE_PROVIDER,
}

export function checkViewState(view: View): ViewState {
  let view_manager = view.view_manager;
  if (view_manager === null) {
    return ViewState.NO_VIEW_MANAGER;
  }

  if (view_manager.getStateManager() === null) {
    return ViewState.NO_STATE_MANAGER;
  }

  if (!view_manager.getStateManager().hasProvider()) {
    return ViewState.NO_STATE_PROVIDER;
  }

  return ViewState.OK;
}

export function drawOverlay(view: View, context: CanvasRenderingContext2D) {
  context.setTransform();
  context.fillStyle = color.black;
  context.fillRect(0, 0, view.width, view.height);
  context.translate(view.width / 2, view.height / 2);
  context.textBaseline = "middle";
  context.textAlign = "center";
  context.font = '12px "Source Code Pro"';
  context.fillStyle = color.white;
  context.fillText("ATTITUDE", 0, 0);
  context.fillText(`[${ViewState[view.state]}]`, 0, 15);
  context.textAlign = "right";
  context.fillText(`VIEW_MANAGER: `, 0, 45);
  context.fillText(`STATE_MANAGER: `, 0, 60);
  context.fillText(`STATE_PROVIDER: `, 0, 75);
  context.textAlign = "left";
  context.fillText((view.view_manager) ? view.view_manager.getName() : "---", 0, 45);
  let state_manager = view.view_manager?.getStateManager();
  context.fillText((state_manager) ? state_manager.getName() : "---", 0, 60);
  context.fillText((state_manager && state_manager.hasProvider()) ? state_manager.getProviderName() : "---", 0, 75);
}

import { State } from "./state.js";
import StateManager from "./state_manager.js";
import StateProvider, { Permissions } from "./state_provider.js";

/**
 * StateManagerSingle holds the state of the aircraft and calls
 * a handler when the state changes.
 */
export default class StateManagerSingle implements StateManager {

  private _name: string;
  private _state: State = new State();
  private _permissions: Permissions = new Permissions();
  private _provider: StateProvider | null = null;
  private _change_handler: State.ChangeHandler;

  constructor(name: string, change_handler: State.ChangeHandler) {
    this._name = name;
    this._change_handler = change_handler;
  }

  getName(): string {
    return this._name;
  }

  getProviderName(): string {
    if (this._provider) {
      return this._provider.getName();
    }
    return "";
  }

  /*
   * The state of the aircraft.
   */
  public get state() : State {
    return this._state;
  }

  public setCameraAttitudeYaw(yaw: number): void {
    if (this._permissions.attitude.level != Permissions.PermissionLevel.READ) {
      this._state.target.camera_attitude.yaw = yaw;
      this._change_handler(State.STATE_TARGET_CAMERA_ATTITUDE)
      this._provider?.applyChangedState();
    }
  }

  public setCameraAttitudePitch(pitch: number): void {
    if (this._permissions.attitude.level != Permissions.PermissionLevel.READ) {
      this._state.target.camera_attitude.pitch = pitch;
      this._change_handler(State.STATE_TARGET_CAMERA_ATTITUDE)
      this._provider?.applyChangedState();
    }
  }

  public setCameraAttitudeYawPitch(yaw: number, pitch: number): void {
    if (this._permissions.attitude.level != Permissions.PermissionLevel.READ) {
      this._state.target.camera_attitude.yaw = yaw;
      this._state.target.camera_attitude.pitch = pitch;
      this._change_handler(State.STATE_TARGET_CAMERA_ATTITUDE)
      this._provider?.applyChangedState();
    }
  }

  public setCameraZoom(zoom: number): void {
    if (this._permissions.zoom.level != Permissions.PermissionLevel.READ) {
      this._state.target.camera_zoom = zoom;
      this._change_handler(State.STATE_TARGET_CAMERA_ZOOM)
      this._provider?.applyChangedState();
    }
  }

  public setCameraFocus(focus: number): void {
    if (this._permissions.focus.level != Permissions.PermissionLevel.READ) {
      this._state.target.camera_focus = focus;
      this._change_handler(State.STATE_TARGET_CAMERA_FOCUS)
      this._provider?.applyChangedState();
    }
  }

  public get permissions() : Permissions {
    return this._permissions;
  }

  hasProvider(): boolean {
    return this._provider !== null;
  }

  registerProvider(provider: StateProvider): void {
    if (provider != null) {
      this.unregisterProvider();
    }
    this._provider = provider;
    this._provider.register(this);

    this._change_handler(State.MANAGEMENT);
  }

  unregisterProvider(): void {
    this._provider?.unregister();
    this._provider = null;
  }

  handlePermissionsChange(permissions: Permissions): void {
    this._permissions = permissions;
  }

  handleStateChanged(): void {
    this._change_handler(State.STATE_CURRENT_CAMERA_ATTITUDE | State.STATE_CURRENT_CAMERA_ZOOM | State.STATE_CURRENT_CAMERA_FOCUS | State.STATE_CURRENT_AIRCRAFT_ATTITUDE);
  }
}

import { color } from "./grafic_elements.js";
import { State } from "./state.js";
import View, { checkViewState, drawOverlay, ViewState } from "./view.js";
import ViewManager from "./view_manager.js";

const ge_indicator_separator = 84;
const ge_indicator_stroke_thickness = 2;
const ge_indicator_visual_thickness = 8;
const ge_indicator_thickness = ge_indicator_visual_thickness - ge_indicator_stroke_thickness;
const ge_indicator_width = 60;
const ge_indicator_height = 28;

const ge_pitch_step = 10;
const ge_pitch_line_small = 20;
const ge_pitch_line_medium = 50;
const ge_pitch_line_large = 90;

const ge_clip_margin = 20;

const ge_tick_width = 18;
const ge_tick_height = 15;

const ge_roll_line_small = 8;
const ge_roll_line_medium = 16;
const ge_roll_tick_width = 12;
const ge_roll_tick_height = 10;

// Field of view in deg (vertical)
const fov = 45;

export default class ViewAttitude extends HTMLCanvasElement implements View {

  private _enabled: boolean = false;
  private _view_manager: ViewManager | null = null;
  private _state: ViewState = ViewState.INIT;

  private _context: CanvasRenderingContext2D;
  private _horizon_length: number = 0;

  private _needs_redraw: boolean = true;

  private _ge_ind: Path2D = new Path2D();
  private _ge_pitch: Path2D = new Path2D();
  private _ge_clip: Path2D = new Path2D();
  private _ge_tick_clip: Path2D = new Path2D();
  private _ge_tick: Path2D = new Path2D();
  private _ge_roll: Path2D = new Path2D();

  constructor() {
    super();

    // Get the 2d drawing context from the canvas.
    let context: CanvasRenderingContext2D | null = this.getContext("2d", { alpha: false });
    if (context === null) {
      throw new Error("context is null");
    }
    this._context = context;

    // Set default values for text drawing.
    this._context.textBaseline = "middle";
    this._context.textAlign = "center";
    this._context.font = '12px "Source Code Pro"';

    // Draw the first frame onto the canvas.
    this.resize();
    this.draw(0);
  }

  public get enabled() : boolean {
    return this._enabled;
  }

  public set enabled(value : boolean) {
    this._enabled = value;

    this.draw(0);
  }

  public get state(): ViewState {
    return this._state;
  }

  public get view_manager(): ViewManager | null {
    return this._view_manager;
  }

  register(manager: ViewManager): void {
    this._view_manager = manager;

    this.handleStateChange(State.MANAGEMENT);
  }

  unregister(): void {
    this._view_manager = null;

    this.handleStateChange(State.MANAGEMENT);
  }

  resize(width?: number, height?: number): void {

    this.width = width || this.offsetWidth;
    this.height = height || this.offsetHeight;

    this._needs_redraw = true;

    // horizon line min length
    this._horizon_length = Math.sqrt(Math.pow(this.width, 2) + Math.pow(this.height, 2));

    // precompute values which are used often in the next section
    let width_2 = this.width / 2;
    let height_2 = this.height / 2;

    let size = Math.min(this.width, this.height);
    let size_2 = size / 2;

    // precalculated paths to be placed onto the canvas

    let ge_indicator_size = Math.max(0.5, Math.min(this.width / 300, 1));
    let ge_indicator_separator_proportional = ge_indicator_size * ge_indicator_separator;
    let ge_indicator_width_proportional = ge_indicator_size * ge_indicator_width;
    let ge_indicator_height_proportional = ge_indicator_size * ge_indicator_height;
    // aircraft indicator
    this._ge_ind = new Path2D(`\
        M ${(this.width - ge_indicator_separator_proportional) / 2} ${(this.height - ge_indicator_thickness) / 2}\
        h ${-ge_indicator_width_proportional}\
        v ${ge_indicator_thickness}\
        h ${ge_indicator_width_proportional - ge_indicator_thickness}\
        v ${ge_indicator_height_proportional - ge_indicator_thickness}
        h ${ge_indicator_thickness} Z
        M ${(this.width + ge_indicator_separator_proportional) / 2} ${(this.height - ge_indicator_thickness) / 2}\
        h ${ge_indicator_width_proportional}\
        v ${ge_indicator_thickness}\
        h ${-ge_indicator_width_proportional + ge_indicator_thickness}\
        v ${ge_indicator_height_proportional - ge_indicator_thickness}
        h ${-ge_indicator_thickness} Z\
        M ${(this.width + ge_indicator_thickness) / 2} ${(this.height - ge_indicator_thickness) / 2}\
        h ${-ge_indicator_thickness} v ${ge_indicator_thickness} h ${ge_indicator_thickness} Z`);

    let ge_pitch_size = Math.max(0.5, Math.min(this.width / 300, 1));
    let ge_pitch_line_small_proportional = ge_pitch_size * ge_pitch_line_small;
    let ge_pitch_line_medium_proportional = ge_pitch_size * ge_pitch_line_medium;
    let ge_pitch_line_large_proportional = ge_pitch_size * ge_pitch_line_large;

    // sing major pitch indication mark with minor steps
    let tmp_ge_pitch = `\
        m ${-ge_pitch_line_small_proportional / 2 - ge_pitch_line_large_proportional / 2} ${ge_pitch_step / 4 / fov * size}\
        h ${ge_pitch_line_large_proportional}\
        m ${-ge_pitch_line_large_proportional / 2 - ge_pitch_line_small_proportional / 2} ${ge_pitch_step / 4 / fov * size}\
        h ${ge_pitch_line_small_proportional}\
        m ${-ge_pitch_line_small_proportional / 2 - ge_pitch_line_medium_proportional / 2} ${ge_pitch_step / 4 / fov * size}\
        h ${ge_pitch_line_medium_proportional}\
        m ${-ge_pitch_line_medium_proportional / 2 - ge_pitch_line_small_proportional / 2} ${ge_pitch_step / 4 / fov * size}\
        h ${ge_pitch_line_small_proportional}`;

    // complete pitch indicator mark strip
    this._ge_pitch = new Path2D(`\
        M ${ge_pitch_line_small_proportional / 2} ${-(90 + (ge_pitch_step / 4)) / fov * size }\
        ` + tmp_ge_pitch.repeat(180 / ge_pitch_step) + `\
        m ${-ge_pitch_line_small_proportional / 2 - ge_pitch_line_large_proportional / 2} ${ge_pitch_step / 4 / fov * size}\
        h ${ge_pitch_line_large_proportional}`);

    // clip path used to leave area of roll indicator free of white lines

    this._ge_clip = new Path2D(`\
        M ${-(Math.cos(Math.PI / 6) * (size_2 - ge_clip_margin))} ${-(Math.sin(Math.PI / 6) * (size_2 - ge_clip_margin))}\
        a ${size_2 - ge_clip_margin} ${size_2 - ge_clip_margin} 0 0 1 ${Math.cos(Math.PI / 6) * (size_2 - ge_clip_margin) * 2} 0\
        L ${size_2} ${0}\
        v ${height_2}\
        h ${-size}\
        v ${-height_2} Z`);

    // clip path to leave roll tick free
    this._ge_tick_clip = new Path2D(`\
        M 0 ${-size_2 + ge_clip_margin}\
        l ${ge_tick_width / 2} ${ge_tick_height}\
        l ${-ge_tick_width} 0 Z\
        M ${-this._horizon_length} ${-this._horizon_length}\
        v ${this._horizon_length * 2}\
        h ${this._horizon_length * 2}\
        v ${-this._horizon_length * 2} Z`);

    // roll indicator tick
    this._ge_tick = new Path2D(`\
        M 0 ${-size_2 + ge_clip_margin}\
        l ${ge_tick_width / 2} ${ge_tick_height}\
        l ${-ge_tick_width} 0 Z`);

    // local function to add the tick marks on the roll indicator strip
    function addTick(angle_degree: number, length: number) {
        let rad = (Math.PI / 2) + (Math.PI * angle_degree / 180);
        return `
            M ${width_2 - (Math.cos(rad) * (size_2 - ge_clip_margin))} ${height_2 - (Math.sin(rad) * (size_2 - ge_clip_margin))}\
            l ${-Math.cos(rad) * length} ${-Math.sin(rad) * length}`;
    }

    // temporary value to compose the path used for the roll indicator
    let tmp_ge_roll = `\
        M ${width_2} ${height_2 - size_2 + ge_clip_margin}\
        l ${ge_roll_tick_width / 2} -${ge_roll_tick_height}\
        l -${ge_roll_tick_width} 0 Z` +
        addTick(-10, ge_roll_line_small) +
        addTick(-20, ge_roll_line_small) +
        addTick(-30, ge_roll_line_medium) +
        addTick(-45, ge_roll_line_small) +
        addTick(-60, ge_roll_line_medium) +
        addTick(10, ge_roll_line_small) +
        addTick(20, ge_roll_line_small) +
        addTick(30, ge_roll_line_medium) +
        addTick(45, ge_roll_line_small) +
        addTick(60, ge_roll_line_medium);

    this._ge_roll = new Path2D(tmp_ge_roll);
  }

  draw(timestamp: number, force_overlay: boolean = false, force_redraw: boolean = false): void {

    if (!this._enabled || this._state != ViewState.OK || force_overlay) {
      drawOverlay(this, this._context);
      return;
    }

    if (!this._needs_redraw && !force_redraw) {
      return;
    }

    // Make sure the state manager is available.
    let state_manager = this.view_manager?.getStateManager();
    if (!state_manager) {
      this._state = checkViewState(this);
      return;
    }

    // Get a local reference to the aircraft state.
    let aircraft_current_state = state_manager.state.current.aircraft_attitude;
    // Calculate the visual size of the drawing elements.
    let size = Math.min(this.width, this.height);

    // Paint the background blue and transform so 0, 0 is in the center of the
    // canvas.
    this._context.setTransform();
    this._context.fillStyle = color.blue_sky;
    this._context.fillRect(0, 0, this.width, this.height);
    this._context.translate(this.width / 2, this.height / 2);

    // Rotate the matrix to the roll value.
    this._context.rotate(aircraft_current_state.roll / 180 * Math.PI);

    // Translate the matrix to pitch value.
    this._context.translate(0, aircraft_current_state.pitch / fov * size);

    // Draw the ground.
    this._context.fillStyle = color.orange_ground;
    this._context.lineWidth = ge_indicator_stroke_thickness;
    this._context.strokeStyle = color.white;
    this._context.fillRect(-this._horizon_length / 2, 0, this._horizon_length, this._horizon_length * 2);

    // Save canvas state without the clip, which is added next.
    this._context.save();
    // Reset transform to draw on screenspace.
    this._context.setTransform();
    // Transform the matrix so 0, 0 is in the center of the canvas.
    this._context.translate(this.width / 2, this.height / 2);

    // Add a clip to the canvas.
    //this.context.fillStyle = color.green;
    this._context.clip(this._ge_clip);

    // Rotate the matrix to the roll value.
    this._context.rotate(aircraft_current_state.roll / 180 * Math.PI);
    // Draw the roll angle tick mark.
    this._context.stroke(this._ge_tick);
    // Add clip which excludes only the tick area from further modification.
    this._context.clip(this._ge_tick_clip);
    // Translate the matrix to pitch value.
    this._context.translate(0, aircraft_current_state.pitch / fov * size);

    // Draw the horizon line
    // The line is clipped on high roll angle.
    this._context.strokeRect(-this._horizon_length / 2, 0, this._horizon_length, this._horizon_length * 2);

    // Draw pitch lines.
    this._context.stroke(this._ge_pitch);

    // Draw the pitch line text marks.
    this._context.fillStyle = color.white;
    this._context.textBaseline = "middle";
    this._context.textAlign = "center";
    this._context.font = '12px "Source Code Pro"';
    // Calculate position and text content of pitch line text marks.
    let base_pitch = (Math.round(aircraft_current_state.pitch / 10) * 10);
    let seperator_size =  Math.max(0.5, Math.min(this.width / 300, 1.8)) * ge_pitch_line_large + 20;
    let seperator = " ".repeat(seperator_size / this._context.measureText(" ").width);
    // Add line markings per major pitch line in view.
    for (let i = Math.max(-90, base_pitch - 20); i < Math.min(100, base_pitch + 30); i+=10) {
        this._context.fillText((i != 0) ? `${Math.abs(i)}${seperator}${Math.abs(i)}` : "", 0, -i / fov * size);
    }

    // Restore state without clip.
    this._context.restore();
    // Reset transform to draw on screenspace.
    this._context.setTransform();

    // Draw roll indicator on screenspace.
    this._context.fillStyle = color.white;
    this._context.fill(this._ge_roll);
    this._context.stroke(this._ge_roll);
    // Draw aircraft indicator on screenspace.
    this._context.fillStyle = color.black;
    this._context.fill(this._ge_ind);
    this._context.stroke(this._ge_ind);

    this._needs_redraw = false;
  }

  handleStateChange(type: State.ChangeType): void {

    if (type & State.MANAGEMENT) {
      this._state = checkViewState(this);
    }

    if (type & State.STATE_CURRENT_AIRCRAFT_ATTITUDE) {
      this._needs_redraw = true;
    }
  }
}

customElements.define("view-attitude", ViewAttitude, { extends: "canvas" });

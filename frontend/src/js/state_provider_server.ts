import StateManager from "./state_manager.js";
import StateProvider, { Permissions } from "./state_provider.js";

export default class StateProviderServer implements StateProvider {

  private name: string;
  private state_manager: StateManager | null = null;
  private websocket: WebSocket | null = null;

  constructor(name: string) {
    this.name = name;
  }

  getName(): string {
    return this.name;
  }

  register(state_manager: StateManager): void {
    this.state_manager = state_manager;
  }

  unregister(): void {
    this.state_manager = null;
  }

  changePermissions(permissions: Permissions): void {
    if (this.websocket != null && this.state_manager != null) {
      let packet = JSON.stringify({"permissions": [
        {level: permissions.attitude.level},
        {level: permissions.zoom.level},
        {level: permissions.focus.level},
        {level: permissions.settings.level}
      ]});
      this.websocket.send(packet);
    }
  }

  applyChangedState() {
    if (this.websocket != null && this.state_manager != null) {
      let packet = JSON.stringify({"target-state": {
        "camera-yaw": Math.floor(this.state_manager.state.target.camera_attitude.yaw * 0xffff / 360),
        "camera-pitch": Math.floor((this.state_manager.state.target.camera_attitude.pitch + 90) * 0xffff / 180),
        "camera-zoom": Math.floor(this.state_manager.state.target.camera_zoom * 0xffff / 100),
        "camera-focus": Math.floor(this.state_manager.state.target.camera_focus * 0xffff / 100),
        "camera-gain-global-analog": Math.floor(this.state_manager.state.target.camera_gain_global_analog),
        "camera-gain-global-digital": Math.floor(this.state_manager.state.target.camera_gain_global_digital),
        "camera-gain-whitebalance-gr": Math.floor(this.state_manager.state.target.camera_gain_whitebalance_gr),
        "camera-gain-whitebalance-r": Math.floor(this.state_manager.state.target.camera_gain_whitebalance_r),
        "camera-gain-whitebalance-b": Math.floor(this.state_manager.state.target.camera_gain_whitebalance_b),
        "camera-gain-whitebalance-gb": Math.floor(this.state_manager.state.target.camera_gain_whitebalance_gb),
        "camera-exposure": Math.floor(this.state_manager.state.target.camera_exposure),
        "camera-gain-whitebalance": Math.floor(this.state_manager.state.target.camera_gain_whitebalance),
        "camera-ir-cut": Math.floor(this.state_manager.state.target.camera_ir_cut),
        "annotation-text": Math.floor(this.state_manager.state.target.annotation_text),
        "camera-reset-focus-zoom": Math.floor(this.state_manager.state.target.camera_reset_focus_zoom),
        "frame-dump-interval": Math.floor(this.state_manager.state.target.frame_dump_interval)
      }});
      this.websocket.send(packet);
    }
  }

  connect(url: string) {
    if (this.websocket != null) {
      this.disconnect();
    }

    this.websocket = new WebSocket(url);
    this.websocket.onmessage = (event: MessageEvent) => {
      let data = JSON.parse(event.data);
      let permissions = new Permissions();
      permissions.attitude = data.permissions[0];
      permissions.zoom = data.permissions[1];
      permissions.focus = data.permissions[2];
      permissions.settings = data.permissions[3];
      this.state_manager?.handlePermissionsChange(permissions);
      if (this.state_manager != null) {
        this.state_manager.state.current.camera_attitude.yaw = data.state.current["camera-yaw"] / 0xffff * 360;
        this.state_manager.state.current.camera_attitude.pitch = data.state.current["camera-pitch"] / 0xffff * 180 - 90;
        this.state_manager.state.current.camera_zoom = data.state.current["camera-zoom"] / 0xffff * 100;
        this.state_manager.state.current.camera_focus = data.state.current["camera-focus"] / 0xffff * 100;
        this.state_manager.state.current.aircraft_attitude.yaw = data.state.current["aircraft-yaw"] / 0xffff * 360;
        this.state_manager.state.current.aircraft_attitude.pitch = data.state.current["aircraft-pitch"] / 0xffff * 180 - 90;
        this.state_manager.state.current.aircraft_attitude.roll = data.state.current["aircraft-roll"] / 0xffff * 360 - 180;
        if (this.state_manager.permissions.attitude.level == Permissions.PermissionLevel.READ) {
          this.state_manager.state.target.camera_attitude.yaw = data.state.target["camera-yaw"] / 0xffff * 360;
          this.state_manager.state.target.camera_attitude.pitch = data.state.target["camera-pitch"] / 0xffff * 180 - 90;
        }
        if (this.state_manager.permissions.zoom.level == Permissions.PermissionLevel.READ) {
          this.state_manager.state.target.camera_zoom = data.state.target["camera-zoom"] / 0xffff * 100;
        }
        if (this.state_manager.permissions.focus.level == Permissions.PermissionLevel.READ) {
          this.state_manager.state.target.camera_focus = data.state.target["camera-focus"] / 0xffff * 100;
        }
        if (this.state_manager.permissions.settings.level == Permissions.PermissionLevel.READ) {
          this.state_manager.state.target.camera_gain_global_analog = data.state.target["camera-gain-global-analog"] / 0xffff * 100;
          this.state_manager.state.target.camera_gain_global_digital = data.state.target["camera-gain-global-digital"] / 0xffff * 100;
          this.state_manager.state.target.camera_gain_whitebalance_gr = data.state.target["camera-gain-whitebalance-gr"] / 0xffff * 100;
          this.state_manager.state.target.camera_gain_whitebalance_r = data.state.target["camera-gain-whitebalance-r"] / 0xffff * 100;
          this.state_manager.state.target.camera_gain_whitebalance_b = data.state.target["camera-gain-whitebalance-b"] / 0xffff * 100;
          this.state_manager.state.target.camera_gain_whitebalance_gb = data.state.target["camera-gain-whitebalance-gb"] / 0xffff * 100;
          this.state_manager.state.target.camera_exposure = data.state.target["camera-exposure"] / 0xffff * 100;
          this.state_manager.state.target.camera_gain_whitebalance = data.state.target["camera-gain-whitebalance"];
          this.state_manager.state.target.camera_ir_cut = data.state.target["camera-ir-cut"];
          this.state_manager.state.target.annotation_text = data.state.target["annotation-text"];
          this.state_manager.state.target.camera_reset_focus_zoom = data.state.target["camera-reset-focus-zoom"];
          this.state_manager.state.target.frame_dump_interval = data.state.target["frame-dump-interval"];
        }
        this.state_manager.handleStateChanged();
      }
    }
  }

  disconnect() {
    if (this.websocket != null) {
      this.websocket.close();
      this.websocket = null;
    }
  }
}

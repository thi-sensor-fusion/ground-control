import { Attitude2D, Attitude3D } from "./utils.js";

/**
 * State stores the state of the Aircraft.
 */
export class State {
  current: State.StateCurrent = new State.StateCurrent();
  target: State.StateTarget = new State.StateTarget();
}
  
export namespace State {
  export class StateCurrent {
    camera_attitude: Attitude2D = new Attitude2D();
    camera_zoom: number = 0;
    camera_focus: number = 0;
    aircraft_attitude: Attitude3D = new Attitude3D();
  }

  export class StateTarget {
    camera_attitude: Attitude2D = new Attitude2D();
    camera_zoom: number = 0;
    camera_focus: number = 0;

    camera_gain_global_analog: number = 0;
    camera_gain_global_digital: number = 0;
    camera_gain_whitebalance_gr: number = 0;
    camera_gain_whitebalance_r: number = 0;
    camera_gain_whitebalance_b: number = 0;
    camera_gain_whitebalance_gb: number = 0;
    camera_exposure: number = 0;
    camera_gain_whitebalance: number = 0;
    camera_ir_cut: number = 0;
    annotation_text: number = 0;
    camera_reset_focus_zoom: number = 0;
    frame_dump_interval: number = 0;
  }

  export type ChangeType = number;

  export const MANAGEMENT: ChangeType = 1 << 0;
  export const STATE_TARGET_CAMERA_ATTITUDE: ChangeType = 1 << 1;
  export const STATE_TARGET_CAMERA_ZOOM: ChangeType = 1 << 2;
  export const STATE_TARGET_CAMERA_FOCUS: ChangeType = 1 << 3;
  export const STATE_CURRENT_CAMERA_ATTITUDE: ChangeType = 1 << 4;
  export const STATE_CURRENT_CAMERA_ZOOM: ChangeType = 1 << 5;
  export const STATE_CURRENT_CAMERA_FOCUS: ChangeType = 1 << 6;
  export const STATE_CURRENT_AIRCRAFT_ATTITUDE: ChangeType = 1 << 7;

  export type ChangeHandler = (type: ChangeType) => void;
}

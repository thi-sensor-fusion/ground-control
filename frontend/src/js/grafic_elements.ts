export namespace color {
  export const white = "#ffffff";
  export const gray_light = "#848484";
  export const gray = "#454545";
  export const gray_metal = "#2A2D34";
  export const gray_dark = "#282828";
  export const black = "#000000";

  export const blue_sky = "#2563f4";
  export const blue_light = "#10cbff";
  export const blue_blue = "#009DDC";

  export const yellow = "#FFB238";

  export const purple = "#f695f6";

  export const green = "#10fa35";

  export const orange = "#ff7433";
  export const orange_ground = "#9E5711";
  export const orange_dark = "#4e402c";
}

export namespace pattern {

  const ge_fence_pattern_size = 6;
}

import { State } from "./state";
import StateProvider, { Permissions } from "./state_provider";

export default interface StateManager {
  
  /**
   * Returns the name of the state manager.
   */
  getName(): string;

  getProviderName(): string;

  get state() : State;
  get permissions(): Permissions;

  /**
   * Returns true if the state manager has a provider registered.
   */
  hasProvider(): boolean;

  /**
   * Register a state provider with the state manager.
   * @param provider is the StateProvider to register.
   */
  registerProvider(provider: StateProvider): void;

  /**
   * This method is called by a registered state provider when the permissions changed.
   * @param permissions is the new Permissions object.
   */
  handlePermissionsChange(permissions: Permissions): void;

  /**
   * This method is called by a registered state provider when the state changed.
   */
  handleStateChanged(): void;

  
  setCameraAttitudeYaw(yaw: number): void;
  setCameraAttitudePitch(pitch: number): void;
  setCameraAttitudeYawPitch(yaw: number, pitch: number): void;
  setCameraZoom(zoom: number): void;
  setCameraFocus(focus: number): void;
}
export enum KeyContinuationMode {
  /** 
   * If a key binding sequence with this continuation mode is matched, no
   * further key bindings are matched in this cycle. This allows for key
   * bindings to prohibit other bindings to be called after this one.
   * Example:
   * pressed_keys: ["a", "b", "c", "d", "e"]
   * key_bindings: [
   *  (keybinding A) {keys: ["a", "b"], continuation: KeyContinuationMode.STOP}
   *  (keybinding B) {keys: ["a", "b", "c"], continuation: KeyContinuationMode.STOP}
   *               ]
   * triggered keybindings: [A]
   */
  STOP,

  /** 
   * Continue looking for another key binding whitch starts the same as the
   * matched key binding.
   * Example:
   * pressed_keys: ["a", "b", "c", "d", "e"]
   * key_bindings: [
   *  (keybinding A) {keys: ["a", "b"], continuation: KeyContinuationMode.CONTINUE}
   *  (keybinding B) {keys: ["a", "b", "c"], continuation: KeyContinuationMode.CONTINUE}
   *               ]
   * triggered keybindings: [A, B]
   */
  CONTINUE,

  /** 
   * Look for a key binding using only the rest of the currently pressed keys.
   * Example:
   * pressed_keys: ["a", "b", "c", "d", "e"]
   * key_bindings: [
   *  (keybinding A) {keys: ["a", "b"], continuation: KeyContinuationMode.RESTART}
   *  (keybinding B) {keys: ["c", "d", "e"], continuation: KeyContinuationMode.RESTART}
   *               ]
   * triggered keybindings: [A, B]
   */
  RESTART,
}

/**
 * Holds a single key binding.
 */
export class KeyBinding {
  keys: Array<string>;
  callback: KeyBinding.EventHandler;
  description: string;
  continuation: KeyContinuationMode;

  constructor(keys: string[], callback: KeyBinding.EventHandler, description: string, continuation: KeyContinuationMode) {
    this.keys = keys;
    this.callback = callback;
    this.description = description;
    this.continuation = continuation;
  }
}

export namespace KeyBinding {
  export type EventHandler = () => void;
}

/**
 * KeyBindingNode forms the tree structure used to dispatch a keypress event to
 * the correct keybindings. A root KeyBindingNode exists as a member in every
 * KeyListener and is used as the first layer of the key - tree structure.
 */
class KeyBindingNode {
  /**
   * When other keys after this key (the key this KeyBindingNode is registered
   * for) can invoke a key binding then a entry for that key is set in the nodes
   * map. The nodes map should be null when no other keys are registered.
   */
  nodes: Map<string, KeyBindingNode> | null = null;

  /**
   * When this field contains a KeyBinding, then its callback will be called.
   */
  key_binding: KeyBinding | null = null;
}

export default class KeyListener {

  /**
   * A flag to keep track whether the key event listener was already registered
   * on the window object.
   */
  private static _registered: boolean = false;

  /**
   * This field contains all currently pressed keys (in order of the time they
   * were pressed).
   */
  private static _map: Map<string, boolean> = new Map<string, boolean>();

  /**
   * Only one KeyListener instance can be active at any given time. The active
   * KeyListener tries to find matching key bindings for the keys in _map. When
   * no key bindings can be found, a possible parent can proccess _map instead.
   */
  private static _active: KeyListener;

  /**
   * This field is a flag indicating whether the _cached_key_event_nodes field
   * is valid.
   */
  private static _cached: boolean = false;

  /**
   * The _cached_key_event_nodes field is a list of cached KeyBindingNodes. The
   * cache is used to improve performance by skipping the search for matching
   * key bindings for repeating events.
   */
  private static _cached_key_event_nodes: KeyBindingNode[] = [];

  /**
   * When a keyup event is triggered for a specific key, but other keys are
   * still pressed, those keys dont receive repeating keydown events anymore.
   * Example: The sequence [a down, b down] produces repeating events.
   *          But [a down, b down, b up] does not.
   * Instead the repeating key bindings are not triggered by the repeating key
   * events but instead by setInterval().
   */
  private static _interval_id: number = -1;

  /**
   * This flags allowes all KeyListener instances to be disabled at once.
   */
  public static enabled: boolean = true;

  /**
   * This field allowes the interval at which repeating key presses are passed
   * to keybinds to be modified.
   */
  public static repeating_interval: number = 75;

  /**
   * This flag allowes this KeyListener instance to be disabled.
   */
  private _enabled: boolean = true;

  /**
   * This field is a reference to a optional KeyListener parent.
   */
  private _parent: KeyListener | null = null;

  /**
   * This is the tree of KeyBindingNodes which map sequences of key presses to
   * KeyBinding instances.
   */
  private _key_bindings: KeyBindingNode = new KeyBindingNode();


  constructor(parent?: KeyListener) {
    // Register the key event listener once.
    if (!KeyListener._registered) {
      window.addEventListener("keydown", KeyListener.handleKeydown);
      window.addEventListener("keyup", KeyListener.handleKeyup);
      KeyListener._registered = true;
      KeyListener._active = this;
    }

    if (parent) {
      this._parent = parent;
    }
  }

  public get enabled(): boolean {
    return this._enabled;
  }

  public set enabled(value: boolean) {
    this._enabled = value;
    KeyListener._cached = false;
  }

  /**
   * Set this KeyListener active.
   */
  setActive(): void {
    KeyListener._active = this;
  }

  /**
   * This is the keydown event handler which processes the keydown events for
   * the active KeyListener.
   */
  static handleKeydown(event: KeyboardEvent): void {
    // Add the newly pressed down key to the map.
    KeyListener._map.set(event.key.toLowerCase(), true);

    // Skip further processing if the entire KeyListener mechanism is disabled.
    if (!KeyListener.enabled) {
      return;
    }
    
    // If the keydown event is repeating, then the repeating interval is
    // started.
    if (event.repeat) {
      if (KeyListener._interval_id === -1) {
        KeyListener._interval_id = setInterval(() => {
          // Handle keypresses using handleKeyPresses() or just call the
          // callback using callCallbacks() depending on the validity of the
          // cache.
          if (KeyListener._cached) {
            KeyListener.callCallbacks();
            return;
          } else {
            KeyListener._cached_key_event_nodes = [];
            KeyListener._active.handleKeyPresses();
          }
        }, KeyListener.repeating_interval);
      }
      // No further processing is needed if the cache is already valid.
      if (KeyListener._cached) {
        return;
      }
    } else {
      // The event is not repeating and the interval will be cleared.
      if (KeyListener._interval_id !== -1) {
        clearInterval(KeyListener._interval_id);
        KeyListener._interval_id = -1;
      }
    }

    // This is not a repeating event or the cache was not valid.
    // The keypresses will be handled using handleKeyPresses() starting at the
    // active KeyListener.
    KeyListener._cached = false;
    KeyListener._cached_key_event_nodes = [];
    KeyListener._active.handleKeyPresses();
    KeyListener._cached = true;
  }

  /**
   * This is the keyup event handler which processes the keyup events for the
   * active KeyListener.
   */
  static handleKeyup(event: KeyboardEvent): void {
    // Remove the newly depressed key from the map.
    KeyListener._map.delete(event.key.toLowerCase());

    // The cache is no longer valid and should be recalculated with the next
    // key press or repeating intervall call.
    KeyListener._cached = false;

    // If this was the last key to be removed from the map the interval is
    // cleared.
    if (KeyListener._map.size === 0) {
      if (KeyListener._interval_id !== -1) {
        clearInterval(KeyListener._interval_id);
        KeyListener._interval_id = -1;
      }
    }
  }

  handleKeyPresses(): void {
    // Start with the root KeyBindingNode of the current KeyListener.
    let current_node: KeyBindingNode = this._key_bindings;
    let next_node: KeyBindingNode | undefined;

    // Only proccess if the KeyListener is enabled.
    if (this._enabled) {
      // Iterate over the current pressed keys in the key map.
      for (const key of KeyListener._map.keys()) {
        // If there are still keys but the current node has no children
        // the rest of the keys are skipped.
        if (current_node.nodes === null) {
          //return;
          break;
        } else {
          // If the current node has a child with the current key.
          if ((next_node = current_node.nodes.get(key)) != undefined) {
            // The child is the next node to be processed.
            current_node = next_node;
            // If the child has a key binding registered it is pushed into the
            // cache.
            if (current_node.key_binding) {
              KeyListener._cached_key_event_nodes.push(current_node);
              // See KeyContinuationMode for the difference between kdifferent
              // modes.
              if (current_node.key_binding.continuation == KeyContinuationMode.STOP) {
                break;
              } else if (current_node.key_binding.continuation == KeyContinuationMode.RESTART) {
                current_node = this._key_bindings;
              }
            }
            continue;
          }
        }
        break;
      }
    }

    // Call The callbacks on all key bindings that where found.
    if (KeyListener._cached_key_event_nodes.length > 0) {
      KeyListener.callCallbacks();
    } else if (this._parent !== null) {
      // If no key bindings were found and a parent is registered, the parent
      // continues to search for key bindings registered there.
      this._parent.handleKeyPresses();
    }
  }

  /**
   * Call the callbacks on all KeyBindingNodes in the cache.
   */
  static callCallbacks(): void {
    KeyListener._cached_key_event_nodes.forEach((key_binding_node) => {
      key_binding_node.key_binding?.callback();
    });
  }

  /**
   * Add a new key binding to the KeyListener.
   * @param key_binding is the KeyBinding to be added.
   * @returns true if the KeyBinding was added successfully and false if not.
   */
  addKeyBinding(key_binding: KeyBinding): boolean {
    // Start with the root KeyBindingNode of the current KeyListener.
    let current_node: KeyBindingNode = this._key_bindings;
    let next_node: KeyBindingNode | undefined;
    
    // Iterate over the keys of the key binding.
    for (const key of key_binding.keys) {
      // If there are still keys but the current node has a empty child map,
      // a new map is added to hold the new child.
      if (current_node.nodes === null) {
        current_node.nodes = new Map<string, KeyBindingNode>();
      } else {
        // If the current node has a child for the current key the node is the
        // next to be processed.
        if ((next_node = current_node.nodes.get(key)) != undefined) {
          current_node = next_node;
          continue;
        }
      }
      // A child map exists but no child with the current key. Instead a new
      // child node is created which is processed next.
      next_node =  new KeyBindingNode();
      current_node.nodes.set(key, next_node);
      current_node = next_node;
    }

    // If after all keys have been processed the current node has a key binding
    // already registered, no new binding can be registered for this key
    // combination and false is returned.
    if (current_node.key_binding != null) {
      return false;
    }

    // Save the passed key binding in the tree and return true.
    current_node.key_binding = key_binding;
    return true;
  }

  /**
   * Remove a key binding from the KeyListener.
   * @param key_binding is the KeyBinding to be removed.
   * @returns true if the KeyBinding was removed successfully and false if not.
   */
  removeKeyBinding(key_binding: KeyBinding): boolean {
    // Start with the root KeyBindingNode of the current KeyListener.
    let current_node: KeyBindingNode = this._key_bindings;
    let next_node: KeyBindingNode | undefined;
    // The path variable is used to delete empty nodes on the path from the
    // root to the key binding.
    let path: KeyBindingNode[] = [];
    // The root node is added to the path as the first node.
    path.push(current_node);
    
    // Iterate over the keys of the key binding.
    for (const key of key_binding.keys) {
      // If there are still keys but the current node has a empty child map,
      // false is returned, as the passed key binding could not be found.
      if (current_node.nodes === null) {
        return false;
      } else {
        // If the current node has a child for the current key the node is the
        // next to be processed. It is also pushed to the path.
        if ((next_node = current_node.nodes.get(key)) != undefined) {
          path.push(next_node);
          current_node = next_node;
          continue;
        }
      }
      // The key did not exist as a child in the current node. The key binding
      // could not be found and false is returned.
      return false;
    }

    // The node with the passed key binding has been found.
    // Remove the reference to the key binding from the node.
    current_node.key_binding = null;
    // Iterate backwards through the path.
    for (let i = path.length - 1; i > 0; --i) {
      // If the current node has no children and no key binding set it can be
      // removed.
      if (path[i].nodes === null && path[i].key_binding === null) {
        // Delete the current node in the parent of the node.
        path[i - 1].nodes?.delete(key_binding.keys[i - 1]);
        // If the parents children list is empty it is set to null.
        if (path[i - 1].nodes?.size === 0) {
          path[i - 1].nodes = null;
        }
      } else {
        // The node was not empty. No further nodes can be removed.
        break;
      }
    }
    // The key binding has been removed and true is returned.
    return true;
  }
}

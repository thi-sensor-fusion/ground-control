
/**
 * Attitude type holding yaw and pitch data.
 */
export class Attitude2D {

  yaw: number;
  pitch: number;

  constructor(yaw: number = 0, pitch: number = 0) {
    this.yaw = yaw;
    this.pitch = pitch;
  }
}

/**
 * Attitude type holding yaw, pitch and roll data.
 */
export class Attitude3D {

  yaw: number;
  pitch: number;
  roll: number;

  constructor(yaw: number = 0, pitch: number = 0, roll: number = 0) {
    this.yaw = yaw;
    this.pitch = pitch;
    this.roll = roll;
  }
}

export enum ViewMode {
  GIMBAL_CURRENT,
  GIMBAL_TARGET,
  AIRCRAFT,
  GROUND,
}

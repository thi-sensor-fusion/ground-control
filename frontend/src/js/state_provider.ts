import StateManager from "./state_manager.js";

export default interface StateProvider {

  /**
   * Returns the name of the state provider.
   */
  getName(): string;

  /**
   * Register the StateProvider with a StateManager.
   * @param state_manager is the StateManager to register this StateProvider with.
   */
  register(state_manager: StateManager): void;

  /**
   * Unregister this StateProvider from a StateManager.
   */
  unregister(): void;

  /**
   * Request to change the permissions of the StateProvider.
   * @param permissions 
   */
  changePermissions(permissions: Permissions): void;

  /**
   * Apply the changes made to the state in the StateManager.
   */
  applyChangedState(): void;

}

export class Permissions {
  attitude: Permissions.Permission = new Permissions.Permission();
  zoom: Permissions.Permission = new Permissions.Permission();
  focus: Permissions.Permission = new Permissions.Permission();
  settings: Permissions.Permission = new Permissions.Permission();
}

export namespace Permissions {

  export class Permission {
    level: PermissionLevel = PermissionLevel.READ;
    accuired: AccuiredType = AccuiredType.NORMAL;
  }

  export enum PermissionLevel {
    READ = 0,
    WRITE = 1,
    FORCED = 2,
  }

  export enum PermissionType {
    ATTITUDE = 0,
    ZOOM = 1,
    FOCUS = 2,
    SETTINGS = 3,
  }

  export enum AccuiredType {
    NORMAL = 0,
    STOLEN = 1,
    ROBBED = 2,
  }

  export type ChangeHandler = (permissions: Permissions) => void;
}

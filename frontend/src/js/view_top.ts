import { color } from "./grafic_elements.js";
import { State } from "./state.js";
import { ViewMode } from "./utils.js";
import View, { checkViewState, drawOverlay, ViewState } from "./view.js";
import ViewManager from "./view_manager.js";

const ge_line_thickness = 1.75;

const ge_aircraft_w = 14;
const ge_aircraft_h = 16;

const ge_compass_radius = 100//140;
const ge_compass_tick_s = 5;
const ge_compass_tick_m = 10;

const ge_bar_padding = 8;
const ge_bar_width = 6;

class ViewTopConfig {
  view_mode: ViewMode = ViewMode.GROUND;
}

class ViewTopCache {
  rotation_view_mode: number = 0;
}

export default class ViewTop extends HTMLCanvasElement implements View {

  private _enabled: boolean = false;
  private _config: ViewTopConfig = new ViewTopConfig();
  private _cache: ViewTopCache = new ViewTopCache();

  private _view_manager: ViewManager | null = null;
  private _state: ViewState = ViewState.INIT;

  private _context: CanvasRenderingContext2D;

  private _needs_redraw: boolean = true;

  private _mouse_active: boolean = false;

  private _ge_aircraft: Path2D = new Path2D();
  private _ge_compass: Path2D = new Path2D();

  private _ge_fence_pattern_orange: CanvasPattern | string;
  private _ge_fence_pattern_green: CanvasPattern | string;

  constructor() {
    super();

    // Get the 2d drawing context from the canvas.
    let context: CanvasRenderingContext2D | null = this.getContext("2d", { alpha: false });
    if (context === null) {
      throw new Error("context is null");
    }
    this._context = context;

    // Set default values for text drawing.
    this._context.textBaseline = "middle";
    this._context.textAlign = "center";
    this._context.font = '12px "Source Code Pro"';

    this._ge_aircraft = new Path2D(`\
        m 0 ${-ge_aircraft_h / 2}\
        l ${ge_aircraft_w / 2} ${ge_aircraft_h}\
        h ${-ge_aircraft_w}\
        Z`);

    const ge_fence_pattern_size = 6;
    const ge_fence_pattern_width = 1.25;

    // create the off-screen canvas
    let patternCanvas = document.createElement("canvas");
    patternCanvas.width = ge_fence_pattern_size;
    patternCanvas.height = ge_fence_pattern_size;
    let patternContext = patternCanvas.getContext("2d");
    if (patternContext === null) {
      throw new Error("context is null");
    }

    // draw pattern to off-screen context
    patternContext.strokeStyle = color.gray_metal;
    patternContext.lineWidth = ge_fence_pattern_width;
    patternContext.beginPath();
    patternContext.moveTo(-ge_fence_pattern_size * .5, ge_fence_pattern_size * .5);
    patternContext.lineTo(ge_fence_pattern_size * .5, -ge_fence_pattern_size * .5);
    patternContext.moveTo(0, ge_fence_pattern_size);
    patternContext.lineTo(ge_fence_pattern_size, 0);
    patternContext.moveTo(ge_fence_pattern_size * .5, ge_fence_pattern_size * 1.5);
    patternContext.lineTo(ge_fence_pattern_size * 1.5, ge_fence_pattern_size * .5);
    patternContext.stroke();
    // create pattern on original canvas
    this._ge_fence_pattern_orange = this._context.createPattern(patternCanvas,"repeat") || "";

    patternCanvas.width = ge_fence_pattern_size;
    patternCanvas.height = ge_fence_pattern_size;

    // draw pattern to off-screen context
    patternContext.strokeStyle = color.green;
    patternContext.lineWidth = ge_fence_pattern_width;
    patternContext.beginPath();
    patternContext.moveTo(-ge_fence_pattern_size * .5, ge_fence_pattern_size * .5);
    patternContext.lineTo(ge_fence_pattern_size * .5, -ge_fence_pattern_size * .5);
    patternContext.moveTo(0, ge_fence_pattern_size);
    patternContext.lineTo(ge_fence_pattern_size, 0);
    patternContext.moveTo(ge_fence_pattern_size * .5, ge_fence_pattern_size * 1.5);
    patternContext.lineTo(ge_fence_pattern_size * 1.5, ge_fence_pattern_size * .5);
    patternContext.stroke();
    // create pattern on original canvas
    this._ge_fence_pattern_green = this._context.createPattern(patternCanvas,"repeat") || "";

    this.addEventListener("mousedown", (e) => {
      if (e.buttons == 1) {
        this._mouse_active = true;

        this._view_manager?.getStateManager().setCameraAttitudeYaw((Math.atan2(e.clientY - this.offsetTop - (this.height / 2), e.clientX - this.offsetLeft - (this.width / 2)) / Math.PI * 180 + 90 + 360 + this._cache.rotation_view_mode) % 360);
      }
    });

    this.addEventListener("mouseup", (e) => {
      this._mouse_active = false;
    });

    this.addEventListener("mouseleave", (e) => {
      this._mouse_active = false;
    });

    this.addEventListener("mousemove", (e) => {
      if (this._mouse_active) {
        this._view_manager?.getStateManager().setCameraAttitudeYaw((Math.atan2(e.clientY - this.offsetTop - (this.height / 2), e.clientX - this.offsetLeft - (this.width / 2)) / Math.PI * 180 + 90 + 360 + this._cache.rotation_view_mode) % 360);
      }
    });

    // Draw the first frame onto the canvas.
    this.resize();
    this.draw(0);
  }

  public get enabled() : boolean {
    return this._enabled;
  }

  public set enabled(value : boolean) {
    this._enabled = value;

    this.draw(0);
  }

  public get state(): ViewState {
    return this._state;
  }

  public get view_manager(): ViewManager | null {
    return this._view_manager;
  }


  register(manager: ViewManager): void {
    this._view_manager = manager;

    this.handleStateChange(State.MANAGEMENT);
  }

  unregister(): void {
    this._view_manager = null;

    this.handleStateChange(State.MANAGEMENT);
  }

  resize(width?: number, height?: number): void {

    this.width = width || this.offsetWidth;
    this.height = height || this.offsetHeight;

    this._needs_redraw = true;

    // precompute values which are used often in the next section
    let width_2 = this.width / 2;
    let height_2 = this.height / 2;

    let size = Math.min(this.width, this.height);
    let size_2 = size / 2;

    // precalculated paths to be placed onto the canvas

    // local function to add the tick marks on the compass rose
    function addTick(angle_degree: number, length: number) {
        let rad = (Math.PI / 2) + (Math.PI * angle_degree / 180)
        return `
            M ${Math.cos(rad) * ge_compass_radius} ${Math.sin(rad) * ge_compass_radius}\
            l ${-Math.cos(rad) * length} ${-Math.sin(rad) * length}`;
    }

    // temporary value to compose the path used for the roll indicator
    let tmp_compass = `\
        M ${-ge_compass_radius} 0\
        a ${ge_compass_radius} ${ge_compass_radius} 0 0 1 ${2 * ge_compass_radius} 0\
        a ${ge_compass_radius} ${ge_compass_radius} 0 0 1 ${-2 * ge_compass_radius} 0`;

    for (let i = 0; i < 36; i++) {
        tmp_compass += addTick(i * 10, (i % 3 == 0) ? ge_compass_tick_m : ge_compass_tick_s);

    }

    this._ge_compass = new Path2D(tmp_compass);
  }

  draw(timestamp: number, force_overlay: boolean = false, force_redraw: boolean = false): void {

    if (!this._enabled || this._state != ViewState.OK || force_overlay) {
      drawOverlay(this, this._context);
      return;
    }

    if (!this._needs_redraw && !force_redraw) {
      return;
    }

    // Make sure the state manager is available.
    let state_manager = this.view_manager?.getStateManager();
    if (!state_manager) {
      this._state = checkViewState(this);
      return;
    }

    let rotation_gimbal_current = state_manager.state.current.camera_attitude.yaw;
    let rotation_gimbal_target = state_manager.state.target.camera_attitude.yaw;
    let rotation_aircraft = state_manager.state.current.aircraft_attitude.yaw;
    let rotation_ground = 0;
    this._cache.rotation_view_mode = 0;

    switch (this._config.view_mode) {
      case ViewMode.GIMBAL_CURRENT: {
        this._cache.rotation_view_mode = rotation_gimbal_current;
      } break;
      case ViewMode.GIMBAL_TARGET: {
        this._cache.rotation_view_mode = rotation_gimbal_target;
      } break;
      case ViewMode.AIRCRAFT: {
        this._cache.rotation_view_mode = rotation_aircraft;
      } break;
    }

    rotation_gimbal_current -= this._cache.rotation_view_mode;
    rotation_gimbal_target -= this._cache.rotation_view_mode;
    rotation_aircraft -= this._cache.rotation_view_mode;
    rotation_ground -= this._cache.rotation_view_mode;

    this._context.textAlign = "center";
    this._context.textBaseline = "middle";

    // transform matrix; 0, 0 is now in the center
    this._context.setTransform();
    this._context.translate(this.width / 2, this.height / 2);
    let transform_matrix = this._context.getTransform();

    // ======== bottom layer ========

    // background
    this._context.fillStyle = color.black;
    this._context.fillRect(-this.width / 2, -this.height / 2, this.width, this.height);

    this._context.fillStyle = this._ge_fence_pattern_orange;
    this._context.strokeStyle = color.gray_metal;
    this._context.beginPath();
    this._context.arc(0, 0, ge_compass_radius, (rotation_aircraft + 90 - 10) / 180 * Math.PI, (rotation_aircraft + 90 + 10) / 180 * Math.PI);
    this._context.lineTo(0, 0);
    this._context.closePath()
    this._context.fill();
    this._context.stroke();

    let camera_current_fov_2 = 180 / state_manager.state.current.camera_zoom / 2;
    this._context.fillStyle = color.green + "45";
    this._context.beginPath();
    this._context.arc(0, 0, ge_compass_radius, (rotation_gimbal_current - 90 - camera_current_fov_2) / 180 * Math.PI, (rotation_gimbal_current - 90 + camera_current_fov_2) / 180 * Math.PI);
    this._context.lineTo(0, 0);
    this._context.closePath();
    this._context.fill();

    // ======== top layer ========

    this._context.setTransform(transform_matrix);
    this._context.rotate(rotation_ground / 180 * Math.PI);

    this._context.strokeStyle = color.white;
    this._context.fillStyle = color.white;
    this._context.lineWidth = 1.1;

    this._context.stroke(this._ge_compass);

    this._context.setTransform(transform_matrix);
    this._context.rotate(rotation_gimbal_current / 180 * Math.PI);
    this._context.strokeStyle = color.green;
    this._context.lineWidth = 1.25;
    this._context.beginPath();
    this._context.moveTo(0, 0);
    this._context.lineTo(0, -ge_compass_radius);
    this._context.stroke();

    let camera_target_fov_2 = 180 / state_manager.state.target.camera_zoom / 2;
    this._context.setTransform(transform_matrix);
    this._context.rotate(rotation_gimbal_target / 180 * Math.PI);
    this._context.beginPath();
    this._context.moveTo(-Math.sin(camera_target_fov_2 / 180 * Math.PI) * ge_compass_radius * 0.8 , -Math.cos(camera_target_fov_2 / 180 * Math.PI) * ge_compass_radius * 0.8);
    this._context.arc(0, 0, ge_compass_radius, (- 90 - camera_target_fov_2) / 180 * Math.PI, (- 90 + camera_target_fov_2) / 180 * Math.PI);
    this._context.lineTo(Math.sin(camera_target_fov_2 / 180 * Math.PI) * ge_compass_radius * 0.8 , -Math.cos(camera_target_fov_2 / 180 * Math.PI) * ge_compass_radius * 0.8);

    this._context.moveTo(0, -ge_compass_radius * 0.5);
    this._context.arc(0, 0, ge_compass_radius * 0.5, -90 / 180 * Math.PI, ((rotation_gimbal_current - rotation_gimbal_target - 90) / 180 * Math.PI), false);
    let arrow_length = Math.max(0, Math.min(Math.abs(rotation_gimbal_target - rotation_gimbal_current) / 5, 7));
    this._context.moveTo(arrow_length, -ge_compass_radius * 0.5 + arrow_length);
    this._context.lineTo(0, -ge_compass_radius * 0.5);
    this._context.lineTo(arrow_length, -ge_compass_radius * 0.5 - arrow_length);

    this._context.moveTo(-8, -ge_compass_radius - 8);
    this._context.lineTo(0, -ge_compass_radius);
    this._context.lineTo(8, -ge_compass_radius - 8);
    this._context.stroke();

    this._context.fillStyle = color.black;
    this._context.strokeStyle = color.white;
    this._context.lineWidth = 1.25;
    this._context.setTransform(transform_matrix);
    this._context.rotate(rotation_aircraft / 180 * Math.PI);
    this._context.beginPath();
    this._context.moveTo(-8, -ge_compass_radius + 8);
    this._context.lineTo(0, -ge_compass_radius);
    this._context.lineTo(8, -ge_compass_radius + 8);
    this._context.stroke();
    this._context.fill(this._ge_aircraft);
    this._context.stroke(this._ge_aircraft);

    this._context.setTransform();
    this._context.fillStyle = color.white;

    for (let i = 0; i < 360; i+=30) {
        let rad = (Math.PI / 2) + (Math.PI * (i + rotation_ground) / 180);
        this._context.fillText(`${i/10}`, this.width / 2 - Math.cos(rad) * (ge_compass_radius + ge_compass_tick_m), this.height / 2 - Math.sin(rad) * (ge_compass_radius + ge_compass_tick_m));
    }

    this._context.strokeStyle = color.green;
    this._context.fillStyle = color.green;
    this._context.textAlign = "right";
    this._context.setTransform(transform_matrix);
    this._context.beginPath();
    this._context.moveTo(this.width / 2 - ge_bar_padding - ge_bar_width, - this.height / 2 + ge_bar_padding);
    this._context.lineTo(this.width / 2 - ge_bar_padding, - this.height / 2 + ge_bar_padding);
    this._context.lineTo(this.width / 2 - ge_bar_padding, this.height / 2 - ge_bar_padding);
    this._context.lineTo(this.width / 2 - ge_bar_padding - ge_bar_width, this.height / 2 - ge_bar_padding);
    this._context.stroke();
    this._context.beginPath();
    this._context.moveTo(this.width / 2 - ge_bar_padding - ge_bar_width, - this.height / 2 + ge_bar_padding + ((this.height - 2 * ge_bar_padding) * state_manager.state.current.camera_zoom / 10));
    this._context.lineTo(this.width / 2 - ge_bar_padding, - this.height / 2 + ge_bar_padding + ((this.height - 2 * ge_bar_padding) * state_manager.state.current.camera_zoom / 10));
    this._context.stroke();
    this._context.beginPath();
    this._context.moveTo(this.width / 2 - ge_bar_padding + ge_bar_width, - ge_bar_width - this.height / 2 + ge_bar_padding + ((this.height - 2 * ge_bar_padding) * state_manager.state.target.camera_zoom / 10));
    this._context.lineTo(this.width / 2 - ge_bar_padding, - this.height / 2 + ge_bar_padding + ((this.height - 2 * ge_bar_padding) * state_manager.state.target.camera_zoom / 10));
    this._context.lineTo(this.width / 2 - ge_bar_padding + ge_bar_width, + ge_bar_width - this.height / 2 + ge_bar_padding + ((this.height - 2 * ge_bar_padding) * state_manager.state.target.camera_zoom / 10));
    this._context.stroke();
    this._context.fillText(`1`, this.width / 2 - ge_bar_padding - ge_bar_width, - this.height / 2 + ge_bar_padding);
    this._context.fillText(`10`, this.width / 2 - ge_bar_padding - ge_bar_width, + this.height / 2 - ge_bar_padding);
    this._context.fillText(`${state_manager.state.target.camera_zoom}`, this.width / 2 - ge_bar_padding - ge_bar_width, - this.height / 2 + ge_bar_padding + ((this.height - 2 * ge_bar_padding) * state_manager.state.target.camera_zoom / 10));

    /*
    this._context.setTransform(transform_matrix);
    this._context.strokeStyle = color.purple;
    this._context.fillStyle = color.black;
    let s_x = Math.sin((rotation_gimbal_target + 0) / 180 * Math.PI);
    let s_y = -Math.cos((rotation_gimbal_target + 0) / 180 * Math.PI);
    let tmp = new Path2D(`
      M ${s_x * ge_compass_radius} ${s_y * ge_compass_radius}\
      l ${s_x * (17)} ${s_y * (17)}\
      l ${s_y * (-8)} ${s_x * (8)}\
      l ${s_x * (16)} ${s_y * (16)}\
      l ${s_y * (16)} ${s_x * (-16)}\
      l ${s_x * (-16)} ${s_y * (-16)}\
      l ${s_y * (-8)} ${s_x * (8)}`);
    this._context.fill(tmp);
    this._context.stroke(tmp);
    this._context.fillStyle = color.purple;
    let rad = (Math.PI / 2) + (Math.PI * (rotation_gimbal_target) / 180);
    this._context.fillText(`A`, - Math.cos(rad) * (ge_compass_radius + 25), - Math.sin(rad) * (ge_compass_radius + 25));

    s_x = Math.sin((100 - this._cache.rotation_view_mode) / 180 * Math.PI);
    s_y = -Math.cos((100 - this._cache.rotation_view_mode) / 180 * Math.PI);
    tmp = new Path2D(`
      M ${s_x * ge_compass_radius} ${s_y * ge_compass_radius}\
      l ${s_x * (17)} ${s_y * (17)}\
      l ${s_y * (-8)} ${s_x * (8)}\
      l ${s_x * (16)} ${s_y * (16)}\
      l ${s_y * (16)} ${s_x * (-16)}\
      l ${s_x * (-16)} ${s_y * (-16)}\
      l ${s_y * (-8)} ${s_x * (8)}`);
    this._context.fillStyle = color.black;
    this._context.fill(tmp);
    this._context.stroke(tmp);
    this._context.fillStyle = color.purple;
    rad = (Math.PI / 2) + (Math.PI * (100 - this._cache.rotation_view_mode) / 180);
    this._context.fillText(`B`, - Math.cos(rad) * (ge_compass_radius + 25), - Math.sin(rad) * (ge_compass_radius + 25));
    */
    //this._context.fillStyle = color.green;
    //let rad = (Math.PI / 2) + (Math.PI * (rotation_gimbal_target) / 180);
    //this._context.fillText(`${Math.round(rotation_gimbal_target)}`, this.width / 2 - Math.cos(rad) * (ge_compass_radius + 30), this.height / 2 - Math.sin(rad) * (ge_compass_radius + 30));

    this._needs_redraw = false;
  }

  handleStateChange(type: State.ChangeType): void {

    if (type & State.MANAGEMENT) {
      this._state = checkViewState(this);
    }

    if (type & (State.STATE_CURRENT_CAMERA_ATTITUDE | State.STATE_CURRENT_CAMERA_ZOOM | State.STATE_CURRENT_CAMERA_FOCUS | State.STATE_TARGET_CAMERA_ATTITUDE | State.STATE_TARGET_CAMERA_ZOOM | State.STATE_TARGET_CAMERA_FOCUS)) {
      this._needs_redraw = true;
    }
  }
}

customElements.define("view-top", ViewTop, { extends: "canvas" });

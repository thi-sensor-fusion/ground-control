"use strict";

class Vec {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    get length() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    scale(factor) {
        this.x *= factor;
        this.y *= factor;
    }

    sub(vec) {
        return new Vec(this.x - vec.x, this.y - vec.y);
    }

    angle(vec) {
        return Math.atan2(vec.y, vec.x) - Math.atan2(this.y,this.x)
    }
}

class MissionItem {
    constructor(pos, type, radius=150) {
        this.pos = pos;
        this.type = type;
        this.radius = radius;
    }
}

class MapObject {
    constructor(pos, type, heading, size) {
        this.pos = pos;
        this.type = type;
        this.heading = heading;
        this.size = size;
    }
}

class GeoFence {
    constructor(points, type, width1, width2, loop=false, min=-Infinity, max=Infinity) {
        this.points = points;
        this.type = type;
        this.width1 = width1;
        this.width2 = width2;
        this.loop = loop;
        this.min = min;
        this.max = max;
    }
}

const ge_line_thickness = 1.75;

const ge_aircraft_w = 14;
const ge_aircraft_h = 16;

const ge_objects_scale = 2;

const ge_marker_scale = ge_objects_scale;
const ge_marker_inner = .5 * ge_marker_scale;
const ge_marker_outer = 1 * ge_marker_scale;

const ge_vertiport_scale = 1;//ge_objects_scale;
const ge_vertiport_width = 1 * ge_vertiport_scale;
const ge_vertiport_v_w = .4 * ge_vertiport_scale;
const ge_vertiport_v_h = .4 * ge_vertiport_scale;

const ge_loop_scale = 1;//ge_objects_scale;
const ge_loop_width = 1 * ge_loop_scale;
const ge_loop_height = .4 * ge_loop_scale;

const ge_compass_radius = 140;
const ge_compass_tick_s = 5;
const ge_compass_tick_m = 10;

const ge_scale_ref = ge_compass_radius;
const ge_screen_ref_h = 4;
const ge_screen_ref_margin = 6;

const ge_realsense_fov = 90;

const ge_ultrasonic_min_dist_size = 150;
const ge_ultrasonic_max_dist = 250;

const ge_fence_pattern_size = 6;
const ge_fence_pattern_width = 1.25;

const ge_grid_spacing = 70;
const ge_grid_count = 50;

const c_white = "#ffffff";
const c_purple = "#f695f6";//"#ff01f2";
const c_blue = "#10cbff";//"#4991f0";
const c_green = "#10fa35";//"#10fa35";//"#85cc12";
const c_orange = "#ff7433";//#e77c25";
const c_dark_gray = "#282828";
const c_gray = "#454545";
const c_light_gray = "#848484";
const c_black = "#000000";
const c_dark_orange = "#4e402c";

class Map {

    static get FENCE_LEFT_SOFT() { return 1 << 0; }
    static get FENCE_LEFT_HARD() { return 1 << 1; }
    static get FENCE_RIGHT_SOFT() { return 1 << 2; }
    static get FENCE_RIGHT_HARD() { return 1 << 3; }

    static get MISSION_TYPE_LANDED() { return 0; }
    static get MISSION_TYPE_FLYING() { return 1; }

    static get OBJECT_TYPE_ARUCO() { return 1 << 0; }
    static get OBJECT_TYPE_LOOP() { return 1 << 1; }
    static get OBJECT_TYPE_VERTIPORT() { return 1 << 2; }

    constructor(canvasElem) {
        // save canvas reference
        this._canvas = canvasElem;
        this._ctx = this._canvas.getContext('2d', { alpha: false });

        this.width = this._canvas.width;
        this.height = this._canvas.height;

        // init state values
        // If true the ground is rotated instead of the aircraft.
        this.rotationRelative = true;
        // Specify the zoom level to use from zoom levels array.
        this._zoom = 2;
        // Specify the scale of each zoom level. The value of each entry is the
        // physical distance ge_scale_ref relates to.
        this._zoomLevels = [250, 400, 600, 750, 1000, 2000, 3000, 4000];
        this._s = ge_scale_ref / this._zoomLevels[this._zoom];
        this.flightStatus = "STARTUP";
        this.flightMode = "TAKEOFF";
        this.autonomous = false;
        this.pos = new Vec(0, 400);
        this.vel = new Vec(0, 200);
        this.acc = new Vec(20, -10);
        this.alt = 0;
        this.heading = 20;

        this._missionItems = [];
        this._mapObjects = [];
        this._geoFences = [];

        this._ultrasonic = [0, 0, 0, 0, 0, 0, 0, 0];
        this._realsense = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        this.disp_vert_ultrasonic = false;
        this.disp_hori_ultrasonic = true;
        this.disp_realsense = true;
        this.disp_geofence = true;
        this.disp_mission = true;
        this.disp_obj = Map.OBJECT_TYPE_ARUCO | Map.OBJECT_TYPE_LOOP | Map.OBJECT_TYPE_VERTIPORT;

        // precompute values which are used often in the next section
        let width_2 = this.width / 2;
        let height_2 = this.height / 2;

        // precalculated paths to be placed onto the canvas

        let tmp_grid_h = `\
            v ${ge_grid_spacing * ge_grid_count}\
            m ${ge_grid_spacing} ${-ge_grid_spacing * ge_grid_count}`;

        let tmp_grid_v = `\
            h ${ge_grid_spacing * ge_grid_count}\
            m ${-ge_grid_spacing * ge_grid_count} ${ge_grid_spacing}`;

        this._ge_grid = new Path2D(`\
            M ${-ge_grid_spacing * ge_grid_count / 2} ${-ge_grid_spacing * ge_grid_count / 2}\
            ` + tmp_grid_h.repeat(ge_grid_count) + `
            M ${-ge_grid_spacing * ge_grid_count / 2} ${-ge_grid_spacing * ge_grid_count / 2}\
            ` + tmp_grid_v.repeat(ge_grid_count));

        this._ge_aircraft = new Path2D(`\
            m 0 ${-ge_aircraft_h / 2}\
            l ${ge_aircraft_w / 2} ${ge_aircraft_h}\
            h ${-ge_aircraft_w}\
            Z`);

        this._ge_marker = new Path2D(`\
            m ${-ge_marker_outer / 2} ${-ge_marker_outer / 2}\
            h ${ge_marker_outer}\
            v ${ge_marker_outer}\
            h ${-ge_marker_outer} Z\
            m ${(ge_marker_outer - ge_marker_inner) / 2} ${(ge_marker_outer - ge_marker_inner) / 2}\
            h ${ge_marker_inner}\
            v ${ge_marker_inner}\
            h ${-ge_marker_inner} Z`);

        this._ge_vertiport = new Path2D(`\
            m ${ge_vertiport_width / 2} 0\
            l ${-ge_vertiport_width / 4} ${-Math.sin(Math.PI / 3) * ge_vertiport_width / 2}\
            h ${-ge_vertiport_width / 2}
            l ${-ge_vertiport_width / 4} ${Math.sin(Math.PI / 3) * ge_vertiport_width / 2}
            l ${ge_vertiport_width / 4} ${Math.sin(Math.PI / 3) * ge_vertiport_width / 2}\
            h ${ge_vertiport_width / 2} Z\
            m ${-ge_vertiport_width / 2 - ge_vertiport_v_w / 2} ${-ge_vertiport_v_h / 2}\
            l ${ge_vertiport_v_w / 2} ${ge_vertiport_v_h}\
            l ${ge_vertiport_v_w / 2} -${ge_vertiport_v_h}`);

        this._ge_loop = new Path2D(`\
            m ${ge_loop_width / 2} 0\
            l ${-Math.cos(Math.PI / 3) * ge_loop_width / 2} ${-Math.sin(Math.PI / 3) * ge_loop_height / 2}\
            h ${-Math.sqrt(Math.pow(ge_loop_width / 2, 2) + Math.pow(ge_loop_height / 4, 2))}
            l ${-Math.cos(Math.PI / 3) * ge_loop_width / 2} ${Math.sin(Math.PI / 3) * ge_loop_height / 2}
            l ${Math.cos(Math.PI / 3) * ge_loop_width / 2} ${Math.sin(Math.PI / 3) * ge_loop_height / 2}\
            h ${Math.sqrt(Math.pow(ge_loop_width / 2, 2) + Math.pow(ge_loop_height / 4, 2))} Z`);

        // local function to add the tick marks on the compass rose
        function addTick(deg, len) {
            let rad = (Math.PI / 2) + (Math.PI * deg / 180)
            return `
                M ${Math.cos(rad) * ge_compass_radius} ${Math.sin(rad) * ge_compass_radius}\
                l ${-Math.cos(rad) * len} ${-Math.sin(rad) * len}`;
        }

        // temporary value to compose the path used for the roll indicator
        let tmp_compass = `\
            M ${-ge_compass_radius} 0\
            a ${ge_compass_radius} ${ge_compass_radius} 0 0 1 ${2 * ge_compass_radius} 0\
            a ${ge_compass_radius} ${ge_compass_radius} 0 0 1 ${-2 * ge_compass_radius} 0`;
            
        for (let i = 0; i < 36; i++) {
            tmp_compass += addTick(i * 10, (i % 3 == 0) ? ge_compass_tick_m : ge_compass_tick_s);
            
        }
    
        this._ge_compass = new Path2D(tmp_compass);

        // setup context state values

        this.setState([0, 0, 0], [0, 0, 0], [0, 0, 0]);

        // set values for text drawing
        this._ctx.textBaseline = "middle";
        this._ctx.textAlign = "right";
        this._ctx.font = "8px";

        this._ge_realsense = new Path2D();

        this._ge_ultrasonic = new Path2D();
        this._ge_ultrasonic_last = new Path2D();
        this._ge_ultrasonic_vertical = new Path2D();

        this._ge_mission = new Path2D();
        this._ge_mission_dash = new Path2D();

        this._ge_objects = new Path2D();

        this._ge_fence_area = new Path2D();
        this._ge_fence_center = new Path2D();
        this._ge_fence_center_highlight = new Path2D();
        this._ge_fence_dash = new Path2D();
        this._ge_fence_solid = new Path2D();

        // create the off-screen canvas
        let patternCanvas = document.createElement("canvas");
        patternCanvas.width = ge_fence_pattern_size;
        patternCanvas.height = ge_fence_pattern_size;
        let patternContext = patternCanvas.getContext("2d");

        // draw pattern to off-screen context
        patternContext.strokeStyle = c_dark_gray;
        patternContext.lineWidth = ge_fence_pattern_width;
        patternContext.beginPath();
        patternContext.moveTo(-ge_fence_pattern_size * .5, ge_fence_pattern_size * .5);
        patternContext.lineTo(ge_fence_pattern_size * .5, -ge_fence_pattern_size * .5);
        patternContext.moveTo(0, ge_fence_pattern_size);
        patternContext.lineTo(ge_fence_pattern_size, 0);
        patternContext.moveTo(ge_fence_pattern_size * .5, ge_fence_pattern_size * 1.5);
        patternContext.lineTo(ge_fence_pattern_size * 1.5, ge_fence_pattern_size * .5);
        patternContext.stroke();
        // create pattern on original canvas 
        this._ge_fence_pattern = this._ctx.createPattern(patternCanvas,"repeat");
    }

    get ultrasonic() {
        return this._ultrasonic;
    }

    set ultrasonic(values) {
        // Save new ultrasonic values and generate path for new values.
        this._ultrasonic = values;
        this._updateUltrasonic();
    }

    get realsense() {
        return this._realsense;
    }

    set realsense(values) {
        // Save new realsense values and generate path for new values.
        this._realsense = values;
        this._updateRealsense();
    }

    get mission() {
        return this._missionItems;
    }

    set mission(values) {
        this._missionItems = values;
        this._updateMission();
    }

    get objects() {
        return this._mapObjects;
    }

    set objects(values) {
        this._mapObjects = values;
        this._updateObjects();
    }

    get fences() {
        return this._geoFences;
    }

    set fences(values) {
        this._geoFences = values;
        this._updateGeoFence();
    }

    get zoom() {
        return this._zoom;
    }

    set zoom(value) {
        // Save new zoom level and calculate new zoom factor.
        this._zoom = Math.min(Math.max(value, 0), this._zoomLevels.length - 1);
        this._s = ge_scale_ref / this._zoomLevels[this._zoom];
        // Update paths of zoom level dependend objects.
        this._updateUltrasonic(true);
        this._updateMission();
        this._updateRealsense();
        this._updateObjects();
        this._updateGeoFence();
    }

    // Set the position, velocity and acceleration (each passed as a list).
    setState(pos, vel, acc) {
        this.pos.y = pos[0]; this.pos.x = pos[1] * -1;
        this.vel.y = vel[0]; this.vel.x = vel[1];
        //this.acc.x = acc[0]; this.acc.y = acc[1];

        this.alt = pos[2];

        this._te_pos = `p:${(pos[0]/100).toFixed(1).padStart(5, " ")},${(pos[1]/100).toFixed(1).padStart(5, " ")},${(pos[2]/100).toFixed(1).padStart(5, " ")}`;
        this._te_vel = `v:${(vel[0]/100).toFixed(1).padStart(5, " ")},${(vel[1]/100).toFixed(1).padStart(5, " ")},${(vel[2]/100).toFixed(1).padStart(5, " ")}`;
        //this._te_acc = `a:${(acc[0]/100).toFixed(1).padStart(5, " ")},${(acc[1]/100).toFixed(1).padStart(5, " ")},${(acc[2]/100).toFixed(1).padStart(5, " ")}`;

        this._updateGeoFenceCenter();
    }

    _updateRealsense() {
        this._ge_realsense = new Path2D();
        let lastInvalid = true;
        let camFov = (Math.PI * ge_realsense_fov / 180);
        let angleStep = camFov / this._realsense.length;
        for (let i = 0; i < this._realsense.length; i++) {
            if  (this._realsense[i] == 0) {
                lastInvalid = true;
                continue;
            }

            if (lastInvalid) {
                this._ge_realsense.moveTo(Math.sin(-camFov / 2 + angleStep * i) * this._realsense[i] * this._s, -Math.cos(-camFov / 2 + angleStep * i) * this._realsense[i] * this._s);
                lastInvalid = false;
            } else {
                this._ge_realsense.lineTo(Math.sin(-camFov / 2 + angleStep * i) * this._realsense[i] * this._s, -Math.cos(-camFov / 2 + angleStep * i) * this._realsense[i] * this._s);
            }
        }
    }

    _updateUltrasonic(clearBoth=false) {
        if (clearBoth) {
            this._ge_ultrasonic_last = new Path2D();
        } else {
            this._ge_ultrasonic_last = this._ge_ultrasonic;
        }
        this._ge_ultrasonic = new Path2D();
        this._ge_ultrasonic_vertical = new Path2D();
        // draw new arcs representing the distance measured by the ultrasonic sensor
        for (let i = 0; i < 6; i++) {
            this._ge_ultrasonic.moveTo(Math.sin(Math.PI / 3 * i) * this._ultrasonic[i] * this._s, -Math.cos(Math.PI / 3 * i) * this._ultrasonic[i] * this._s);
            this._ge_ultrasonic.arc(0, 0, this._ultrasonic[i] * this._s, Math.PI * 1.5 + Math.PI / 3 * i, Math.PI * 1.5 + Math.PI / 3 * (i + 1));
        }
        let size = (ge_ultrasonic_max_dist - Math.min(ge_ultrasonic_max_dist, this._ultrasonic[6])) * (ge_ultrasonic_min_dist_size / ge_ultrasonic_max_dist);
        size *= this._s;
        this._ge_ultrasonic_vertical.moveTo(-size / 2, -size / 2);
        this._ge_ultrasonic_vertical.lineTo(-size / 2, size / 2);
        this._ge_ultrasonic_vertical.lineTo(size / 2, size / 2);
        this._ge_ultrasonic_vertical.lineTo(size / 2, -size / 2);
        this._ge_ultrasonic_vertical.closePath();
        size = (ge_ultrasonic_max_dist - Math.min(ge_ultrasonic_max_dist, this._ultrasonic[7])) * (ge_ultrasonic_min_dist_size / ge_ultrasonic_max_dist);
        size *= this._s;
        this._ge_ultrasonic_vertical.moveTo(0, -size / 2);
        this._ge_ultrasonic_vertical.lineTo(-size / 2, size / 2);
        this._ge_ultrasonic_vertical.lineTo(size / 2, size / 2);
        this._ge_ultrasonic_vertical.closePath();
    }

    _updateMission() {
        this._ge_mission = new Path2D();
        this._ge_mission_dash = new Path2D();

        this._missionItems.forEach(item => {
            if (item.type === Map.MISSION_TYPE_LANDED) {
                this._ge_mission.moveTo((item.pos.x + item.radius) * this._s, -item.pos.y * this._s);
                this._ge_mission.arc(item.pos.x * this._s, -item.pos.y * this._s, item.radius * this._s, 0, Math.PI * 2);
            } else {
                this._ge_mission_dash.moveTo((item.pos.x + item.radius) * this._s, -item.pos.y * this._s);
                this._ge_mission_dash.arc(item.pos.x * this._s, -item.pos.y * this._s, item.radius * this._s, 0, Math.PI * 2);
            }
            
        });

        for (let i = 1; i < this._missionItems.length; i++) {
            let rel = this._missionItems[i].pos.sub(this._missionItems[i - 1].pos);
            rel.scale(1 / rel.length);
            this._ge_mission_dash.moveTo((this._missionItems[i - 1].pos.x + rel.x * this._missionItems[i - 1].radius) * this._s, -(this._missionItems[i - 1].pos.y + rel.y * this._missionItems[i - 1].radius) * this._s);
            this._ge_mission_dash.lineTo((this._missionItems[i].pos.x - rel.x * this._missionItems[i].radius) * this._s, -(this._missionItems[i].pos.y - rel.y * this._missionItems[i].radius) * this._s);
        }
    }

    _updateObjects() {
        this._ge_objects = new Path2D();

        let m = new DOMMatrix();
        let m_reset = new DOMMatrix();

        this._mapObjects.forEach(item => {
            if (!(item.type & this.disp_obj)) {
                return;
            }
            m.setMatrixValue(m_reset);
            m.translateSelf(item.pos.x * this._s, -item.pos.y * this._s);
            m.rotateSelf(0, 0, item.heading);
            m.scaleSelf(item.size * this._s, item.size * this._s, 1);
            switch (item.type) {
                case Map.OBJECT_TYPE_ARUCO:
                    this._ge_objects.addPath(this._ge_marker, m);
                    break;
                case Map.OBJECT_TYPE_LOOP:
                    this._ge_objects.addPath(this._ge_loop, m);
                    break;
                case Map.OBJECT_TYPE_VERTIPORT:
                    this._ge_objects.addPath(this._ge_vertiport, m);
                    break;
            
                default:
                    break;
            }
        });
    }

    _updateGeoFence() {
        this._ge_fence_area = new Path2D();
        this._ge_fence_dash = new Path2D();
        this._ge_fence_solid = new Path2D();

        function calcCorner(p, v1, v2, offset) {
            let angle = Math.PI / 2 - ((Math.PI - v1.angle(v2)) / 2);
            v1.scale(1 / v1.length);
            let vOrth = new Vec(-v1.y, v1.x);
            return new Vec(
                p.x + Math.tan(angle) * offset * v1.x - offset * vOrth.x,
                p.y + Math.tan(angle) * offset * v1.y - offset * vOrth.y
            );
        }

        function calcEnd(p, v1, offset) {
            v1.scale(1 / v1.length);
            let vOrth = new Vec(-v1.y, v1.x);
            return new Vec(
                p.x - offset * vOrth.x, 
                p.y - offset * vOrth.y
            );
        }

        let drawOffsetLine = (dst, points, loop, offset) => {
            if (points.length > 1) {

                let lastRel;
                let thisRel;
                let pos;
                // first element
                thisRel = points[1].sub(points[0]);
                lastRel = points[0].sub(points[points.length - 1]);
                if (loop) {
                    pos = calcCorner(points[0], lastRel, thisRel, offset);
                    dst.moveTo(pos.x * this._s, -pos.y * this._s);
                } else {
                    pos = calcEnd(points[0], thisRel, offset);
                    dst.lineTo(pos.x * this._s, -pos.y * this._s);
                }
                lastRel = thisRel;

                // all other elements
                for (let i = 1; i < points.length - 1; i++) {
                    thisRel = points[i + 1].sub(points[i]);
    
                    pos = calcCorner(points[i], lastRel, thisRel, offset);
                    dst.lineTo(pos.x * this._s, -pos.y * this._s);
                    lastRel = thisRel;
                }
                // last element
                if (loop) {
                    thisRel = points[0].sub(points[points.length - 1]);
                    pos = calcCorner(points[points.length - 1], lastRel, thisRel, offset);
                    dst.lineTo(pos.x * this._s, -pos.y * this._s);
                    dst.closePath();
                } else {
                    pos = calcEnd(points[points.length - 1], thisRel, offset);
                    dst.lineTo(pos.x * this._s, -pos.y * this._s);
                }  
            }
        }

        this._geoFences.forEach(f => {

            this._ge_fence_area.moveTo(f.points[0].x * this._s, -f.points[0].y * this._s);
            drawOffsetLine(this._ge_fence_area, f.points, f.loop, f.width1);
            drawOffsetLine(this._ge_fence_area, f.points.slice().reverse(), f.loop, f.width2);
            this._ge_fence_area.closePath();

            if (f.type & (Map.FENCE_LEFT_SOFT | Map.FENCE_LEFT_HARD)) {
                let active = (f.type & Map.FENCE_LEFT_SOFT) ? this._ge_fence_dash : this._ge_fence_solid;
                let pos = calcEnd(f.points[0], f.points[1].sub(f.points[0]), f.width1);
                active.moveTo(pos.x * this._s, -pos.y * this._s);
                drawOffsetLine(active, f.points, f.loop, f.width1);
            }

            if (f.type & (Map.FENCE_RIGHT_SOFT | Map.FENCE_RIGHT_HARD)) {
                let active = (f.type & Map.FENCE_RIGHT_SOFT) ? this._ge_fence_dash : this._ge_fence_solid;
                let pos = calcEnd(f.points[f.points.length - 1], f.points[f.points.length - 2].sub(f.points[f.points.length - 1]), f.width2);
                active.moveTo(pos.x * this._s, -pos.y * this._s);
                drawOffsetLine(active, f.points.slice().reverse(), f.loop, f.width2);
            }
        });

        this._updateGeoFenceCenter();
    }

    _updateGeoFenceCenter() {
        this._ge_fence_center = new Path2D();
        this._ge_fence_center_highlight = new Path2D();

        this._geoFences.forEach(f => {

            let active = ((f.min <= this.alt) && (this.alt <= f.max)) ? this._ge_fence_center_highlight : this._ge_fence_center;
            let first = true;
            f.points.forEach(item => {
                if (first) {
                    active.moveTo(item.x * this._s, -item.y * this._s);
                    first = false;
                } else {
                    active.lineTo(item.x * this._s, -item.y * this._s);
                }
            });
            if (f.loop) {
                active.closePath();
            }
        });
    }

    draw() {
        let compassRotation = 0;
        this._ctx.lineWidth = 1;
        this._ctx.textAlign = "center";

        // transform matrix; 0, 0 is now in the center
        this._ctx.translate(this.width / 2, this.height / 2);

        // ======== bottom layer ========

        // background
        this._ctx.fillStyle = c_black;
        this._ctx.fillRect(-this.width / 2, -this.height / 2, this.width, this.height);

        this._ctx.save();

        if (this.rotationRelative) {
            this._ctx.rotate(-Math.PI * this.heading / 180);
            compassRotation = this.heading;
        }
        this._ctx.translate(this.pos.x * this._s, this.pos.y * this._s);


        this._ctx.strokeStyle = c_dark_orange;
        this._ctx.stroke(this._ge_grid);
        this._ctx.lineWidth = ge_line_thickness;
        this._ctx.strokeStyle = c_light_gray;

        let matrix = new DOMMatrix();
        this._ge_fence_pattern.setTransform(matrix.rotate(this.heading));

        if (this.disp_geofence) {
            this._ctx.fillStyle = this._ge_fence_pattern;
            this._ctx.fill(this._ge_fence_area);
            this._ctx.stroke(this._ge_fence_center_highlight);
            this._ctx.strokeStyle = c_gray;
            this._ctx.stroke(this._ge_fence_center);
            this._ctx.stroke(this._ge_fence_solid);
            this._ctx.setLineDash([6, 8]);
            this._ctx.stroke(this._ge_fence_dash);
        }
        if (this.disp_mission) {
            this._ctx.setLineDash([6, 8]);
            this._ctx.strokeStyle = c_purple;
            this._ctx.stroke(this._ge_mission_dash);
            this._ctx.setLineDash([]);
            this._ctx.stroke(this._ge_mission);
        }
        this._ctx.setLineDash([]);
        this._ctx.strokeStyle = c_green;
        this._ctx.stroke(this._ge_objects);

        // ======== drone layer ========


        this._ctx.restore();
        if (!this.rotationRelative) {
            this._ctx.save();
            this._ctx.rotate(Math.PI * this.heading / 180);
            //this._ctx.translate(0, 40);
        }

        this._ctx.strokeStyle = c_blue;
        this._ctx.setLineDash([2, 4]);
        this._ctx.lineWidth = 1.25;
        this._ctx.stroke(this._ge_ultrasonic_last);
        this._ctx.lineWidth = ge_line_thickness;
        this._ctx.setLineDash([]);

        if (this.disp_hori_ultrasonic) {
            this._ctx.stroke(this._ge_ultrasonic);
        }
        if (this.disp_vert_ultrasonic) {
            this._ctx.stroke(this._ge_ultrasonic_vertical);
        }
        this._ctx.strokeStyle = c_orange;
        if (this.disp_realsense) {
            this._ctx.stroke(this._ge_realsense);
        }

        this._ctx.strokeStyle = c_white;
        this._ctx.setLineDash([8, 6]);
        this._ctx.beginPath();
        this._ctx.moveTo(0, 0);
        let timestep = 0.5; // time in seconds
        let p = new Vec(0, 0);
        let v = new Vec(this.vel.x, this.vel.y);
        //for (let i = 0; i < 10; i++) {
        //    p.x += (this.acc.x / 2 * timestep + v.x) * timestep;
        //    p.y += (this.acc.y / 2 * timestep + v.y) * timestep;
        //    v.x += this.acc.x * timestep;
        //    v.y += this.acc.y * timestep;
        //    this._ctx.lineTo(p.x * this._s, -p.y * this._s);   
        //}
        this._ctx.stroke();
        this._ctx.setLineDash([]);

        this._ctx.fill(this._ge_aircraft);
        this._ctx.stroke(this._ge_aircraft);


        // ======== top layer ========
        if (!this.rotationRelative) {
            this._ctx.restore();
        } else {
            this._ctx.rotate(-Math.PI * this.heading / 180);
        }

        this._ctx.fillStyle = c_white;
        this._ctx.strokeStyle = c_white;
        this._ctx.lineWidth = 1;

        this._ctx.stroke(this._ge_compass);

        this._ctx.setTransform();

        for (let i = 0; i < 360; i+=30) {
            let rad = (Math.PI / 2) + (Math.PI * (i - compassRotation) / 180);
            this._ctx.fillText(`${i/10}`, this.width / 2 - Math.cos(rad) * (ge_compass_radius + ge_compass_tick_m), this.height / 2 - Math.sin(rad) * (ge_compass_radius + ge_compass_tick_m));
        }

        this._ctx.textAlign = "left";

        this._ctx.beginPath();
        this._ctx.moveTo(this.width - ge_screen_ref_margin - 0.5, this.height - ge_screen_ref_margin - ge_screen_ref_h);
        this._ctx.lineTo(this.width - ge_screen_ref_margin - 0.5, this.height - ge_screen_ref_margin - 0.5);
        this._ctx.lineTo(this.width - ge_screen_ref_margin - Math.floor(1000 * this._s) - 0.5, this.height - ge_screen_ref_margin - 0.5);
        this._ctx.lineTo(this.width - ge_screen_ref_margin - Math.floor(1000 * this._s) - 0.5, this.height - ge_screen_ref_margin - ge_screen_ref_h);
        this._ctx.stroke();


        this._ctx.font = '11px "Source Code Pro"';

        this._ctx.fillText(`${this._zoom}/${this._zoomLevels[this._zoom] / 100}m`, ge_screen_ref_margin, this.height - 24)
        this._ctx.fillText((this.rotationRelative) ? "rel" : "abs", ge_screen_ref_margin, 12)

        // print other information
        this._ctx.textAlign = "right";
        this._ctx.fillText(this.flightMode + " | " + this.flightStatus, this.width - ge_screen_ref_margin, 12)
        this._ctx.fillText(this._te_pos, this.width - ge_screen_ref_margin, 24)
        this._ctx.fillText(this._te_vel, this.width - ge_screen_ref_margin, 36)
        //this._ctx.fillText(this._te_acc, this.width - ge_screen_ref_margin, 48)
        this._ctx.fillText(this.autonomous ? "[auto]" : "[manual]", this.width - ge_screen_ref_margin, 48)
        this._ctx.font = "10px sans-serif";
    }

}

export default Map;
export {Vec, MissionItem, MapObject, GeoFence};

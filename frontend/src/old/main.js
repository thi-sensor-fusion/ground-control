import Map, {MissionItem, Vec, MapObject, GeoFence} from "./map.js";

const color_white = "#ffffff";
const color_purple = "#f695f6";//"#ff01f2";
const color_blue = "#10cbff";//"#4991f0";
const color_green = "#10fa35";//"#10fa35";//"#85cc12";
const color_orange = "#ff7433";//#e77c25";
const color_dark_gray = "#282828";
const color_gray = "#454545";
const color_light_gray = "#848484";
const color_black = "#000000";
const color_dark_orange = "#4e402c";

const path_camera_size = 20;
const path_camera_dash_length = 30;
const path_camera = new Path2D(`\
m ${-path_camera_size / 2} ${-path_camera_size / 2}\
h ${path_camera_dash_length} m ${(path_camera_size - 2 * path_camera_dash_length)} 0 h ${path_camera_dash_length}\
v ${path_camera_dash_length} m 0 ${(path_camera_size - 2 * path_camera_dash_length)} v ${path_camera_dash_length}\
h ${-path_camera_dash_length} m ${-(path_camera_size - 2 * path_camera_dash_length)} 0 h ${-path_camera_dash_length}\
v ${-path_camera_dash_length} m 0 ${-(path_camera_size - 2 * path_camera_dash_length)} v ${-path_camera_dash_length}`);


const ge_grid_spacing = 70;
const ge_grid_count = 50;
let tmp_grid_h = `\
v ${ge_grid_spacing * ge_grid_count}\
m ${ge_grid_spacing} ${-ge_grid_spacing * ge_grid_count}`;

let tmp_grid_v = `\
h ${ge_grid_spacing * ge_grid_count}\
m ${-ge_grid_spacing * ge_grid_count} ${ge_grid_spacing}`;

const path_grid = new Path2D(`\
M ${-ge_grid_spacing * ge_grid_count / 2} ${-ge_grid_spacing * ge_grid_count / 2}\
` + tmp_grid_h.repeat(ge_grid_count) + `
M ${-ge_grid_spacing * ge_grid_count / 2} ${-ge_grid_spacing * ge_grid_count / 2}\
` + tmp_grid_v.repeat(ge_grid_count));

class ControlVisual extends HTMLCanvasElement {

  constructor() {
    super();

    this._ctx = this.getContext("2d");

    this._payload_heading = 0; // 0-360 in deg
    this._payload_pitch = 0; // -90-90 in deg

    this._camera_target_heading = 0; // 0-360 in deg
    this._camera_target_pitch = 0; // -90-90 in deg

    this._camera_target_zoom = 1; // 1-?
    this._camera_target_fov = 90; // fov in deg

    this._camera_current_heading = 100; // 0-360 in deg
    this._camera_current_pitch = 30; // -90-90 in deg

    this._camera_current_zoom = 1; // 1-?
    this._camera_current_fov = 90; // fov in deg

    this.tmp_path_camera = new Path2D();
    this.x = 0;

    this._mouse_active = false;
    this._keypress_count = 0;

    this.addEventListener("mousedown", (e) => {
      if (e.buttons == 1) {
        this._mouse_active = true;
      if (e.buttons == 1) {
        this._camera_target_heading = (e.clientX - this.offsetLeft) / this.offsetWidth * 360 - 180;
        this._camera_target_pitch = (e.clientY - this.offsetTop) / this.offsetHeight * 90;
      }
      }
    });

    this.addEventListener("mouseup", (e) => {
      this._mouse_active = false;
    });

    this.addEventListener("mouseleave", (e) => {
      this._mouse_active = false;
    });

    this.addEventListener("mousemove", (e) => {
      if (this._mouse_active) {
        this._camera_target_heading = (e.clientX - this.offsetLeft) / this.offsetWidth * 360 - 180;
        this._camera_target_pitch = (e.clientY - this.offsetTop) / this.offsetHeight * 90;
      }
    });

    document.addEventListener("keydown", (e) => {
      ++this._keypress_count;
      let amount = (0 + Math.min(5, Math.log(Math.max(this._keypress_count * 1.5 - 0, 0))));
      //console.log(amount);
      switch (e.key) {
        case "w":
          this._camera_target_pitch = Math.max(0, this._camera_target_pitch - amount);
          break;
        case "s":
          this._camera_target_pitch = Math.min(90, this._camera_target_pitch + amount);
          break;
        case "a":
          this._camera_target_heading = (this._camera_target_heading - amount + 180 + 360) % 360 - 180;
          break;
        case "d":
          this._camera_target_heading = (this._camera_target_heading + amount + 180 + 360) % 360 - 180;
          break;
      }
    });

    document.addEventListener("keyup", (e) => {
      this._keypress_count = 0;
    });

    //this.addEventListener("keypress", (e) => {
    //  console.log(e);
    //});

    const ge_fence_pattern_size = 6;
    const ge_fence_pattern_width = 1.25;

    // create the off-screen canvas
    let patternCanvas = document.createElement("canvas");
    patternCanvas.width = ge_fence_pattern_size;
    patternCanvas.height = ge_fence_pattern_size;
    let patternContext = patternCanvas.getContext("2d");

    // draw pattern to off-screen context
    patternContext.strokeStyle = color_orange;//color_dark_gray;
    patternContext.lineWidth = ge_fence_pattern_width;
    patternContext.beginPath();
    patternContext.moveTo(-ge_fence_pattern_size * .5, ge_fence_pattern_size * .5);
    patternContext.lineTo(ge_fence_pattern_size * .5, -ge_fence_pattern_size * .5);
    patternContext.moveTo(0, ge_fence_pattern_size);
    patternContext.lineTo(ge_fence_pattern_size, 0);
    patternContext.moveTo(ge_fence_pattern_size * .5, ge_fence_pattern_size * 1.5);
    patternContext.lineTo(ge_fence_pattern_size * 1.5, ge_fence_pattern_size * .5);
    patternContext.stroke();
    // create pattern on original canvas
    this._ge_fence_pattern_orange = this._ctx.createPattern(patternCanvas,"repeat");

    // draw pattern to off-screen context
    patternContext.strokeStyle = color_dark_gray;
    patternContext.lineWidth = ge_fence_pattern_width;
    patternContext.beginPath();
    patternContext.moveTo(-ge_fence_pattern_size * .5, ge_fence_pattern_size * .5);
    patternContext.lineTo(ge_fence_pattern_size * .5, -ge_fence_pattern_size * .5);
    patternContext.moveTo(0, ge_fence_pattern_size);
    patternContext.lineTo(ge_fence_pattern_size, 0);
    patternContext.moveTo(ge_fence_pattern_size * .5, ge_fence_pattern_size * 1.5);
    patternContext.lineTo(ge_fence_pattern_size * 1.5, ge_fence_pattern_size * .5);
    patternContext.stroke();
    // create pattern on original canvas
    this._ge_fence_pattern_gray = this._ctx.createPattern(patternCanvas,"repeat");


    this._tmp_counter = 0;
  }

  draw() {

    let path_camera_size = Math.sin(this.x / 10) * 40 + 100; //20;
    this.x++;
    let path_camera_dash_length = 20;
    this.tmp_path_camera_target = new Path2D(`\
    m ${-path_camera_size / 2} ${-path_camera_size / 2}\
    h ${path_camera_dash_length} m ${(path_camera_size - 2 * path_camera_dash_length)} 0 h ${path_camera_dash_length}\
    v ${path_camera_dash_length} m 0 ${(path_camera_size - 2 * path_camera_dash_length)} v ${path_camera_dash_length}\
    h ${-path_camera_dash_length} m ${-(path_camera_size - 2 * path_camera_dash_length)} 0 h ${-path_camera_dash_length}\
    v ${-path_camera_dash_length} m 0 ${-(path_camera_size - 2 * path_camera_dash_length)} v ${-path_camera_dash_length}`);
    this.tmp_path_camera_current = new Path2D(`\
    m ${-path_camera_size / 2} ${-path_camera_size / 2}\
    v ${path_camera_size} h ${path_camera_size} v -${path_camera_size} h -${path_camera_size}\
    m ${path_camera_size / 2 - 10} ${path_camera_size / 2} h 20 m -10 -10 v 20\
    m ${path_camera_size / 2 + 15} ${-10 - path_camera_size / 2} h -5 v ${path_camera_size} h 5 m 0 ${-path_camera_size * ((path_camera_size - 60) / 80)} h -5\
    m ${- path_camera_size - 25} ${path_camera_size * ((path_camera_size - 60) / 80)} h 5 v ${-path_camera_size} h -5 m 0 ${path_camera_size * ((path_camera_size - 60) / 80)} h 5`);
    let rel_heading = (this._camera_target_heading - this._camera_current_heading) / 360 * 1024;
    let rel_pitch = (this._camera_target_pitch - this._camera_current_pitch) / 90 * 512;
    let rel_length = Math.sqrt(Math.pow(rel_heading, 2) + Math.pow(rel_pitch, 2));
    let rel_x = -rel_heading / rel_length * 10 +10;
    let rel_y = -rel_pitch / rel_length * 10;
    this.tmp_path_camera_connect = new Path2D(`\
    m ${rel_heading -10} ${rel_pitch}\
    a 10 10 0 0 0 20 0 a 10 10 0 0 0 -20 0 m ${rel_x} ${rel_y} l ${-rel_heading - rel_x + 10} ${-rel_pitch - rel_y}`);

    this._ctx.translate(this.width / 2 + 0.5, this.height / 2 + 0.5);
    this._ctx.fillStyle = color_black;

    this._ctx.fillRect(-this.width / 2, -this.height / 2, this.width, this.height);

    this._ctx.lineWidth = 1;
    this._ctx.strokeStyle = color_dark_orange;
    this._ctx.stroke(path_grid);


    this._ctx.beginPath();
    this._ctx.moveTo(this.width / 2, -this.height / 2);
    this._ctx.lineTo(-this.width / 2, -this.height / 2);
    for (let i = 0; i < 81; ++i) {
      //this._ctx.lineTo(this.width * (i / 20) - this.width / 2, (Math.cos(i / 20 * 2 * Math.PI) * 40) -this.height / 3);
      let c1_cos = Math.cos(i / 80 * 2 * Math.PI);
      let c1_sin = Math.sin(i / 80 * 2 * Math.PI);
      let c2_cos = Math.cos(this._payload_pitch / 90 * Math.PI) * c1_sin;
      let c2_sin = Math.sin(this._payload_pitch / 90 * Math.PI) * c1_sin;
      let x = Math.atan2(c1_cos, c2_cos);
      let y = Math.atan2(Math.sqrt(c1_cos * c1_cos, c2_cos), c2_sin);
      this._ctx.lineTo(this.width * (y / Math.PI) - this.width / 2, (x * 40) -this.height / 2);
    }
    this._ctx.lineWidth = 1.75;
    if (this._camera_target_pitch < 10 && this._tmp_counter < 3) {
      this._ctx.strokeStyle = color_orange;
      this._ctx.fillStyle = this._ge_fence_pattern_orange;
    } else {
      this._ctx.strokeStyle = color_gray;
      this._ctx.fillStyle = this._ge_fence_pattern_gray;
    }
    this._tmp_counter = (this._tmp_counter + 1) % 9;
    this._ctx.stroke();
    this._ctx.fill();

    this._ctx.beginPath();
    this._ctx.moveTo(this.width / 4, -this.height / 2);
    this._ctx.lineTo(this.width / 4, this.height / 2);
    this._ctx.strokeStyle = color_purple;
    this._ctx.stroke();

    this._ctx.strokeStyle = color_green;
    this._ctx.lineWidth = 1.25;
    this._ctx.save();
    this._ctx.translate(this._camera_target_heading / 360 * 1024, this._camera_target_pitch / 90 * 512 - 256);
    this._ctx.stroke(this.tmp_path_camera_target);
    this._ctx.restore();
    this._ctx.translate(this._camera_current_heading / 360 * 1024, this._camera_current_pitch / 90 * 512 - 256);
    this._ctx.save();
    let clip_path = new Path2D();
    clip_path.rect(-2000, -2000, 4000, 4000);
    clip_path.rect(-path_camera_size / 2, -path_camera_size / 2, path_camera_size, path_camera_size);
    this._ctx.clip(clip_path, "evenodd");
    this._ctx.stroke(this.tmp_path_camera_connect);
    this._ctx.restore();
    this._ctx.stroke(this.tmp_path_camera_current);

    this._ctx.setTransform();
  }
}

customElements.define("control-visual", ControlVisual, { extends: "canvas" });

map = null

window.onload = () => {

  //setInterval(()=> {
  //  document.getElementById("control-visual").draw();
  //}, 50);

  map = new Map(document.getElementById("map"))
  map.fences = [
    new GeoFence(
      [
        new Vec(-1000, -500),
        new Vec(-500, 500),
        new Vec(500, 500),
        new Vec(1000, -500)
      ],
      Map.FENCE_LEFT_SOFT, 100, 100, true),
    new GeoFence(
      [
        new Vec(400, 0),
        new Vec(200, -200),
        new Vec(0, -200),
        new Vec(-200, 0),
        new Vec(-200, 800),
        new Vec(0, 1000),
        new Vec(200, 1000),
        new Vec(400, 800)
      ],
    Map.FENCE_RIGHT_HARD, 100, 50, false, -1000, 200),
  ];
  map.mission = [
    new MissionItem(new Vec(0, 200), 0, 150),
    new MissionItem(new Vec(-350, 900), 1, 100),
    new MissionItem(new Vec(500, 700), 1, 50),
    new MissionItem(new Vec(600, 0), 0, 200)
  ];

  map.objects = [
    new MapObject(new Vec(0, 200), 1, 0, 200),
    new MapObject(new Vec(600, 0), 1, 0, 200),
    new MapObject(new Vec(-175, 550), 2, -30, 200),

    new MapObject(new Vec(-0, -0), 0, 20, 100),
    new MapObject(new Vec(100, -200), 0, 45, 50),
    new MapObject(new Vec(-300, -300), 0, 0, 40),
    new MapObject(new Vec(130, -400), 0, -20, 30)
  ];


  map.draw();
}


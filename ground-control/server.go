package ground_control

import (
	"net"
	"net/http"
	"sync"

	log "github.com/sirupsen/logrus"
)

type StateTarget struct {
	CameraYaw   uint16 `json:"camera-yaw"`
	CameraPitch uint16 `json:"camera-pitch"`
	CameraZoom  uint16 `json:"camera-zoom"`
	CameraFocus uint16 `json:"camera-focus"`

	CameraGainGlobalAnalog   uint16 `json:"camera-gain-global-analog"`
	CameraGainGlobalDigital  uint16 `json:"camera-gain-global-digital"`
	CameraGainWhitebalanceGr uint16 `json:"camera-gain-whitebalance-gr"`
	CameraGainWhitebalanceR  uint16 `json:"camera-gain-whitebalance-r"`
	CameraGainWhitebalanceB  uint16 `json:"camera-gain-whitebalance-b"`
	CameraGainWhitebalanceGb uint16 `json:"camera-gain-whitebalance-gb"`
	CameraExposure           uint16 `json:"camera-exposure"`
	CameraGainWhitebalance   uint8  `json:"camera-gain-whitebalance"`
	CameraIrCut              uint8  `json:"camera-ir-cut"`
	AnnotationText   		 uint8  `json:"annotation-text"`
	CameraResetFocusZoom     uint8  `json:"camera-reset-focus-zoom"`
	FrameDumpInterval     	 uint16 `json:"frame-dump-interval"`
}

type StateCurrent struct {
	CameraYaw     uint16 `json:"camera-yaw"`
	CameraPitch   uint16 `json:"camera-pitch"`
	CameraZoom    uint16 `json:"camera-zoom"`
	CameraFocus   uint16 `json:"camera-focus"`
	AircraftYaw   uint16 `json:"aircraft-yaw"`
	AircraftPitch uint16 `json:"aircraft-pitch"`
	AircraftRoll  uint16 `json:"aircraft-roll"`
}

type State struct {
	Current StateCurrent `json:"current"`
	Target  StateTarget  `json:"target"`
}

type PermissionLevel uint8

const (
	// The FrontendConnection is only allowed to receive the aircraft state.
	Read PermissionLevel = 0
	// The FrontendConnection is allowed to write to the aircraft state. This
	// permission might get stolen by another FrontendConnection.
	Write PermissionLevel = 1
	// The FrontendConnection is allowed to write to the aircraft state. No
	// other FrontendConnection is able to steal this permission.
	Forced PermissionLevel = 2
)

type PermissionType uint8

const (
	// Allowes access to the attitude of the camera.
	Attitude PermissionType = 0
	// Allowes access to the zoom of the camera.
	Zoom PermissionType = 1
	// Allowes access to the focus of the camera.
	Focus PermissionType = 2
	// Allowes access to the settings of the camera.
	Settings PermissionType = 3
)

type AccuiredType uint8

const (
	// The permission level was accuired normaly without stealing from another
	// connection.
	Normal AccuiredType = 0
	// The permission level was stolen from another connection.
	Stolen AccuiredType = 1
	// The connection got robbed from its original permission level by another
	// connection. As a result the permission level is now downgraded.
	Robbed AccuiredType = 2
)

type Permission struct {
	// The current permission level.
	Level PermissionLevel `json:"level"`
	// Accuired represents how the permission level was accuired.
	Accuired AccuiredType `json:"accuired"`
}

type Permissions [4]Permission
type PermissionsOwner [4]*FrontendConnection

// // Server forwards json messages from a websocket connection to the
// // specified ConfigurationSocket.
type Server struct {
	// ServerAddress specifies the ip address of the udp connection
	// to the aircraft.
	ServerAddress string
	// AircraftAddress specifies the ip address of the aircraft.
	AircraftAddress string

	// FrontendAddress optionally specifies the ip address for the frontend http
	// server to listen on. This http server provides a handle to establish a
	// websocket connection. If no value is specified 0.0.0.0:80 will be used.
	FrontendAddress string

	// Path to the directory to serve the frontend from.
	FrontendDirectory string

	// Path to serve the frontend on.
	FrontendPath string

	// Path to serve the websocket on.
	FrontendWebsocketPath string

	// Log level. One of [panic, fatal, error, warn, info, debug, trace]
	LogLevel string

	// The state of the aircraft.
	state State

	// This mutex is used to disallow concurrent access to the state.
	stateMutex sync.RWMutex

	// The aircraft udp connection instance.
	aircraftConnection *net.UDPConn

	// The http server which provides a handle to establish a websocket
	// connection.
	frontendListener *http.Server

	// A map of all valid frontend connections.
	frontendConnections map[uint]*FrontendConnection

	// This mutex is used to disallow concurrent access to the map of
	// frontend sockets.
	frontendMutex sync.Mutex

	// The index of the next frontend connection to be established.
	frontendConnectionsNewIndex uint

	// The current write permission owner for the different permission types.
	writePermissionOwner PermissionsOwner

	// The quit channel is used to signal a server stop to all goroutines by
	// closing the channel.
	quit chan struct{}

	// WaitGroup to wait for all goroutines to finish before exiting.
	wg sync.WaitGroup
}

// Run the Server instance.
func (s *Server) Run() {
	// Initalize logger
	log.SetFormatter(&log.TextFormatter{
		FullTimestamp:          true,
		TimestampFormat:        "02.01.2006 15:04:05",
		DisableLevelTruncation: true,
		PadLevelText:           true,
	})

	if s.LogLevel != "" {
		level, err := log.ParseLevel(s.LogLevel)
		if err == nil {
			log.SetLevel(level)
		}
	}

	s.quit = make(chan struct{})

	// Set default values.
	if s.FrontendAddress == "" {
		s.FrontendAddress = ":80"
	}
	if s.FrontendDirectory == "" {
		s.FrontendDirectory = "./frontend/"
	}
	if s.FrontendPath == "" {
		s.FrontendPath = "/"
	}
	if s.FrontendWebsocketPath == "" {
		s.FrontendWebsocketPath = "/websocket"
	}

	// Create aircraft connection and frontend listener.
	s.createAircraftConnection()
	s.createFrontendListener()
}

// Stop the server instance.
func (s *Server) Stop() {
	log.Info("[server] Shutting down.")
	// Close the quit channel signaling all goroutines to stop and exit.
	close(s.quit)

	// Stop serving frontend and the aircraft connection.
	s.stopAircraftConnection()
	s.stopFrontend()

	s.wg.Wait()
	log.Info("[server] Shut down.")
}

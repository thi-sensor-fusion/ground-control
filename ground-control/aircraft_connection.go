package ground_control

import (
	"encoding/binary"
	"net"
	"os"
	"syscall"

	log "github.com/sirupsen/logrus"
)

// Create the aircraft connection listener socket and start serving.
func (s *Server) createAircraftConnection() {
	// Resolve the passed server address.
	serverAddress, err := net.ResolveUDPAddr("udp4", s.ServerAddress)
	if err != nil {
		log.Panicf("[aircraft] Error while resolving server address: %s", err.Error())
	}
	// Resolve the passed aircraft address.
	aircraftAddress, err := net.ResolveUDPAddr("udp4", s.AircraftAddress)
	if err != nil {
		log.Panicf("[aircraft] Error while resolving aircraft address: %s", err.Error())
	}

	// Create a udp listener on the configuration address.
	s.aircraftConnection, err = net.DialUDP("udp", serverAddress, aircraftAddress)
	if err != nil {
		log.Fatalf("[aircraft] Error while creating aircraft connection: %s", err.Error())
	}

	log.Infof("[aircraft] Communicating with aircraft at %s via %s.", s.AircraftAddress, s.ServerAddress)

	// Start listenAircraft() as goroutine and wait for it to exit once
	// wg.Done() is called.
	s.wg.Add(1)
	go s.listenAircraft()
}

func (s *Server) listenAircraft() {
	defer s.aircraftConnection.Close()
	defer s.wg.Done()

	var buffer [1024]byte

	for {
		// Wait for a new connection on configuration listener.
		n, _, err := s.aircraftConnection.ReadFromUDP(buffer[:])
		if err != nil {
			// A error indicates either that the listener socket has been closed
			// by the Stop() method and the goroutine should terminate or a
			// general error.
			select {
			case <-s.quit:
				log.Info("[aircraft] Closing aircraft socket.")
				return
			default:
				if e, ok := err.(*net.OpError); ok {
					if e, ok := e.Err.(*os.SyscallError); ok {
						if e, ok := e.Err.(syscall.Errno); ok {
							if e == syscall.ECONNREFUSED {
								log.Debug("[aircraft] Connection refused.")
								continue
							}
						}
					}
				}
				log.Errorf("[aircraft] Error while accepting connection: %s", err.Error())
			}
		} else {
			// Ignore udp packet if it is shorter than expected.
			if n < 14 {
				continue
			}

			s.decode(buffer[:])
		}
	}
}

func (s *Server) stopAircraftConnection() {
	log.Info("[aircraft] Closing aircraft connection.")
	// Close the configuration listener socket.
	s.aircraftConnection.Close()
}

func (s *Server) decode(buffer []byte) {
	// Unpack data into current state struct.
	s.stateMutex.Lock()
	s.state.Current.CameraYaw = binary.BigEndian.Uint16(buffer[0:])
	s.state.Current.CameraPitch = binary.BigEndian.Uint16(buffer[2:])
	s.state.Current.CameraZoom = binary.BigEndian.Uint16(buffer[4:])
	s.state.Current.CameraFocus = binary.BigEndian.Uint16(buffer[6:])
	s.state.Current.AircraftYaw = binary.BigEndian.Uint16(buffer[8:])
	s.state.Current.AircraftPitch = binary.BigEndian.Uint16(buffer[10:])
	s.state.Current.AircraftRoll = binary.BigEndian.Uint16(buffer[12:])
	s.stateMutex.Unlock()

	// Write the changed state to each frontendConnection.
	s.frontendMutex.Lock()
	for _, frontendSocket := range s.frontendConnections {
		s.frontendSocketWrite(frontendSocket, false)
	}
	s.frontendMutex.Unlock()
}

func (s *Server) send() {
	var buffer [28]byte
	// Pack the data from the current state struct into the buffer.
	s.stateMutex.RLock()
	binary.BigEndian.PutUint16(buffer[0:], s.state.Target.CameraYaw)
	binary.BigEndian.PutUint16(buffer[2:], s.state.Target.CameraPitch)
	binary.BigEndian.PutUint16(buffer[4:], s.state.Target.CameraZoom)
	binary.BigEndian.PutUint16(buffer[6:], s.state.Target.CameraFocus)
	binary.BigEndian.PutUint16(buffer[8:], s.state.Target.CameraGainGlobalAnalog)
	binary.BigEndian.PutUint16(buffer[10:], s.state.Target.CameraGainGlobalDigital)
	binary.BigEndian.PutUint16(buffer[12:], s.state.Target.CameraGainWhitebalanceGr)
	binary.BigEndian.PutUint16(buffer[14:], s.state.Target.CameraGainWhitebalanceR)
	binary.BigEndian.PutUint16(buffer[16:], s.state.Target.CameraGainWhitebalanceB)
	binary.BigEndian.PutUint16(buffer[18:], s.state.Target.CameraGainWhitebalanceGb)
	binary.BigEndian.PutUint16(buffer[20:], s.state.Target.CameraExposure)

	buffer[22] = s.state.Target.CameraGainWhitebalance
	buffer[23] = s.state.Target.CameraIrCut
	buffer[24] = s.state.Target.AnnotationText
	buffer[25] = s.state.Target.CameraResetFocusZoom
	binary.BigEndian.PutUint16(buffer[26:], s.state.Target.FrameDumpInterval)
	s.stateMutex.RUnlock()

	// Write the buffer to the aircraft.
	s.aircraftConnection.Write(buffer[:])
}

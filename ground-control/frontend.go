package ground_control

import (
	"context"
	"encoding/json"
	"io"
	"net"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"

	log "github.com/sirupsen/logrus"
)

// FrontendConnection holds the information of a single frontend
// connection.
type FrontendConnection struct {
	id uint
	// The http websocket connection.
	conn *websocket.Conn

	m sync.Mutex

	// The current permission level of the frontend connection.
	Permissions Permissions
}

type FrontendReceivePacket struct {
	Permissions *Permissions `json:"permissions,omitempty"`

	State StateTarget `json:"target-state"`

	RequestClients bool `json:"list-clients"`
}

type FrontendSendPacketClient struct {
	Ip string `json:"ip"`

	Permissions Permissions `json:"permissions"`
}

type FrontendSendPacket struct {
	Permissions Permissions `json:"permissions"`

	State State `json:"state"`

	Id uint `json:"id"`

	Clients map[uint]FrontendSendPacketClient `json:"clients"`
}

// Create the frontend listener and start serving.
func (s *Server) createFrontendListener() {
	log.Infof("[frontend] Listening for frontend instances on %s.", s.FrontendAddress)

	// Create a new http router to handle all http requests.
	r := mux.NewRouter()

	// Create websocket handler route.
	r.HandleFunc(s.FrontendWebsocketPath, s.handleFrontendWebsocket)
	r.PathPrefix(s.FrontendPath).Handler(http.StripPrefix(s.FrontendPath, http.FileServer(http.Dir(s.FrontendDirectory))))

	// Create http server instance
	s.frontendListener = &http.Server{
		Handler:      r,
		Addr:         s.FrontendAddress,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	// Initialize frontend connections map.
	s.frontendConnections = make(map[uint]*FrontendConnection)
	s.frontendConnectionsNewIndex = 0

	// Start the http server in a new goroutine.
	s.wg.Add(1)
	go func() {
		if err := s.frontendListener.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("[frontend] Error while creating listener: %s\n", err)
		}
		s.wg.Done()
	}()
}

func (s *Server) handleFrontendWebsocket(w http.ResponseWriter, r *http.Request) {
	var upgrader = websocket.Upgrader{}
	log.Debugf("[frontend] New connection attempt from %s.", r.RemoteAddr)
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Warnf("[frontend] Error during connection upgradation with %s: %+v", r.RemoteAddr, err)
		return
	}

	// Lock frontendSocket mutex and check if it valid.
	s.frontendMutex.Lock()

	// Set frontendConnection to default values.
	frontendConnection := &FrontendConnection{
		id:   s.frontendConnectionsNewIndex,
		conn: conn,
		Permissions: Permissions{
			Permission{Level: Read, Accuired: Normal},
			Permission{Level: Read, Accuired: Normal},
			Permission{Level: Read, Accuired: Normal},
			Permission{Level: Read, Accuired: Normal},
		},
	}
	// Add the new frontendConnection to the frontendConnections map.
	s.frontendConnections[s.frontendConnectionsNewIndex] = frontendConnection
	s.frontendConnectionsNewIndex++

	// Write a list of all connections to each connection.
	for _, connection := range s.frontendConnections {
		s.frontendSocketWrite(connection, true)
	}

	s.frontendMutex.Unlock()

	// Handle the created socket connection
	s.wg.Add(1)
	s.handleFrontendSocket(frontendConnection)

}

func (s *Server) handleFrontendSocket(frontendConnection *FrontendConnection) {
	defer frontendConnection.conn.Close()
	defer s.wg.Done()

	log.Debugf("[frontend] New client connected from %s.", frontendConnection.conn.RemoteAddr().String())

	var packet FrontendReceivePacket

	for {
		// Read frontend websocket data into ClientPacket
		err := frontendConnection.conn.ReadJSON(&packet)
		// If an error occured the socket is marked as invalid and the mutex is
		// unlocked.
		if err != nil {
			// Test for json SyntaxError; only log a warning and continue
			if err == io.EOF {
				// Close frontend socket on error.
				log.Debugf("[frontend] Closing socket for %s.", frontendConnection.conn.RemoteAddr().String())
			} else {
				select {
				case <-s.quit:
					log.Debugf("[frontend] Closing socket for %s.", frontendConnection.conn.RemoteAddr().String())
					break
				default:
					if e, ok := err.(*json.SyntaxError); ok {
						log.Warnf("[frontend] JSON error from %s: %+v", frontendConnection.conn.RemoteAddr().String(), e)
						continue
					} else if _, ok := err.(*net.OpError); ok {
						log.Debugf("[frontend] Closing socket for %s.", frontendConnection.conn.RemoteAddr().String())
					} else if _, ok := err.(*websocket.CloseError); ok {
						log.Debugf("[frontend] Closing socket for %s.", frontendConnection.conn.RemoteAddr().String())
					} else {
						log.Errorf("[frontend] Error from %s: %+v", frontendConnection.conn.RemoteAddr().String(), err)
						continue
					}
				}
			}
			break
		}

		s.handleFrontendSocketRead(frontendConnection, &packet)
	}

	// The frontend connection has been closed or become invalid.
	// The connection will be deleted and a list of all connections will be
	// written to each connection.
	s.frontendMutex.Lock()
	delete(s.frontendConnections, frontendConnection.id)
	for _, connection := range s.frontendConnections {
		s.frontendSocketWrite(connection, true)
	}
	s.frontendMutex.Unlock()
}

func (s *Server) handleFrontendSocketRead(frontend_connection *FrontendConnection, packet *FrontendReceivePacket) {
	s.frontendMutex.Lock()
	frontend_connection.m.Lock()

	var permission_changed = false

	// Apply requested permissions if they are available and do not conflict
	// with other connections.
	if packet.Permissions != nil {
		permission_changed = s.setPermission(frontend_connection, packet.Permissions, Attitude) || permission_changed
		permission_changed = s.setPermission(frontend_connection, packet.Permissions, Zoom) || permission_changed
		permission_changed = s.setPermission(frontend_connection, packet.Permissions, Focus) || permission_changed
		permission_changed = s.setPermission(frontend_connection, packet.Permissions, Settings) || permission_changed
	}

	// Apply the state changes which are valid for the current permissions of
	// the connection.
	s.stateMutex.Lock()
	if frontend_connection.Permissions[Attitude].Level != Read {
		s.state.Target.CameraYaw = packet.State.CameraYaw
		s.state.Target.CameraPitch = packet.State.CameraPitch
	}
	if frontend_connection.Permissions[Zoom].Level != Read {
		s.state.Target.CameraZoom = packet.State.CameraZoom
	}
	if frontend_connection.Permissions[Focus].Level != Read {
		s.state.Target.CameraFocus = packet.State.CameraFocus
	}
	if frontend_connection.Permissions[Settings].Level != Read {
		s.state.Target.CameraFocus = packet.State.CameraFocus
		s.state.Target.CameraGainGlobalAnalog = packet.State.CameraGainGlobalAnalog
		s.state.Target.CameraGainGlobalDigital = packet.State.CameraGainGlobalDigital
		s.state.Target.CameraGainWhitebalanceGr = packet.State.CameraGainWhitebalanceGr
		s.state.Target.CameraGainWhitebalanceR = packet.State.CameraGainWhitebalanceR
		s.state.Target.CameraGainWhitebalanceB = packet.State.CameraGainWhitebalanceB
		s.state.Target.CameraGainWhitebalanceGb = packet.State.CameraGainWhitebalanceGb
		s.state.Target.CameraExposure = packet.State.CameraExposure
		s.state.Target.CameraGainWhitebalance = packet.State.CameraGainWhitebalance
		s.state.Target.CameraIrCut = packet.State.CameraIrCut
		s.state.Target.AnnotationText = packet.State.AnnotationText
		s.state.Target.CameraResetFocusZoom = packet.State.CameraResetFocusZoom
		s.state.Target.FrameDumpInterval = packet.State.FrameDumpInterval
	}
	s.stateMutex.Unlock()

	frontend_connection.m.Unlock()
	s.frontendMutex.Unlock()

	s.send()

	if permission_changed {
		// When the permissions of the connection changed, a list of all
		// connections will be written to each connection.
		s.frontendMutex.Lock()
		for _, connection := range s.frontendConnections {
			s.frontendSocketWrite(connection, true)
		}
		s.frontendMutex.Unlock()
	} else if packet.RequestClients {
		// A list of all connections will be written to the current connection
		// if it was requested in the packet.
		s.frontendSocketWrite(frontend_connection, true)
	}
}

func (s *Server) setPermission(connection *FrontendConnection, permission *Permissions, permission_type PermissionType) bool {
	var changed = false
	// Skip further permission processing if no change was requested.
	if permission[permission_type].Level != connection.Permissions[permission_type].Level {
		switch permission[permission_type].Level {
		case Read:
			{
				// A change to read permission is always possible.
				if s.writePermissionOwner[permission_type] == connection {
					s.writePermissionOwner[permission_type] = nil
				}
				connection.Permissions[permission_type].Level = Read
				connection.Permissions[permission_type].Accuired = Normal
				changed = true
				break
			}
		case Write:
			{
				// A change to write permission is only possible if there is no
				// current forced owner for that permission type.
				if s.writePermissionOwner[permission_type] != nil {
					if s.writePermissionOwner[permission_type].Permissions[permission_type].Level != Forced {
						// The current owner for this permission type is robbed
						// of its permission.
						s.writePermissionOwner[permission_type].Permissions[permission_type].Level = Read
						s.writePermissionOwner[permission_type].Permissions[permission_type].Accuired = Robbed
						connection.Permissions[permission_type].Accuired = Stolen
					} else {
						// The current permission owner has permission level
						// forced. As a result no permission change is
						// performed.
						break
					}
				} else {
					connection.Permissions[permission_type].Accuired = Normal
				}
				connection.Permissions[permission_type].Level = Write
				s.writePermissionOwner[permission_type] = connection
				changed = true
				break
			}
		case Forced:
			{
				// A change to forced permission is only possible if there is
				// no current forced owner for that permission type.
				if s.writePermissionOwner[permission_type] != nil {
					if s.writePermissionOwner[permission_type].Permissions[permission_type].Level != Forced {
						// The current owner for this permission type is robbed
						// of its permission.
						s.writePermissionOwner[permission_type].Permissions[permission_type].Level = Read
						s.writePermissionOwner[permission_type].Permissions[permission_type].Accuired = Robbed
						connection.Permissions[permission_type].Accuired = Stolen
					} else {
						// The current permission owner has permission level
						// forced. As a result no permission change is
						// performed.
						break
					}
				} else {
					connection.Permissions[permission_type].Accuired = Normal
				}
				connection.Permissions[permission_type].Level = Forced
				s.writePermissionOwner[permission_type] = connection
				changed = true
				break
			}
		}
	}
	return changed
}

func (s *Server) frontendSocketWrite(connection *FrontendConnection, clients_list bool) {
	connection.m.Lock()
	s.stateMutex.RLock()
	// Create a map of structs with the ip address and the permission
	// information of each frontendConnection.
	var clients = make(map[uint]FrontendSendPacketClient, len(s.frontendConnections))
	for i, frontendConnection := range s.frontendConnections {
		clients[i] = FrontendSendPacketClient{
			Ip:          frontendConnection.conn.RemoteAddr().String(),
			Permissions: frontendConnection.Permissions,
		}
	}
	// Create packet and write as json through the websocket connection.
	var packet = FrontendSendPacket{
		Permissions: connection.Permissions,
		State:       s.state,
		Id:          connection.id,
		Clients:     clients,
	}
	s.stateMutex.RUnlock()
	connection.conn.WriteJSON(packet)
	connection.m.Unlock()
}

func (s *Server) stopFrontend() {

	// Shutdown the http server.
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		cancel()
	}()

	log.Info("[frontend] Closing listener socket.")
	if err := s.frontendListener.Shutdown(ctx); err != nil {
		log.Fatalf("[frontend] Closing listener socket failed: %+v", err)
	}

	// Close existing websocket connection.
	log.Info("[frontend] Closing instance sockets.")
	s.frontendMutex.Lock()
	for _, frontendSocket := range s.frontendConnections {
		log.Debugf("[frontend] Closing instance socket %s.", frontendSocket.conn.RemoteAddr().String())
		frontendSocket.conn.Close()
	}
	s.frontendMutex.Unlock()
}

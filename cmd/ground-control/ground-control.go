package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	ground_control "gitlab.com/thi-sensor-fusion/ground-control/ground-control"
)

func main() {

	signals := make(chan os.Signal, 1)

	server := ground_control.Server{}

	flag.StringVar(&server.ServerAddress, "server-address", ":50043", "Address and port to listen for the aircraft on.")
	flag.StringVar(&server.AircraftAddress, "aircraft-address", ":50042", "Address and port of the aircraft.")
	flag.StringVar(&server.FrontendAddress, "frontend-address", ":8080", "Address and port for frontend http server to listen on.")
	flag.StringVar(&server.FrontendDirectory, "frontend-directory", "./frontend/", "Directory to serve as frontend.")
	flag.StringVar(&server.FrontendPath, "frontend-path", "/", "URL path under which the frontend will be served.")
	flag.StringVar(&server.FrontendWebsocketPath, "frontend-websocket-path", "/websocket", "URL path under which the frontend websocket will be serverd.")
	flag.StringVar(&server.LogLevel, "log-level", "info", "Leg level to use when printing logs.")

	flag.Parse()

	server.Run()

	signal.Notify(signals, syscall.SIGINT, syscall.SIGUSR1)

	<-signals

	fmt.Print("\r")

	server.Stop()
}

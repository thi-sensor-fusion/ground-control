# Download dependencies.
dep:
	@go mod download

# Format the go source code according to standard style.
format:
	@go fmt $$(go list ./... | grep -v /vendor/)

# Test for common mistakes in the source.
vet:
	@go vet $$(go list ./... | grep -v /vendor/)

# Run all defined unit (*_test.go) and integration (_test/) tests.
test:
	@go test -race $$(go list ./... | grep -v /vendor/)

# Run all defined unit (*_test.go) and integration (_test/) tests.
test-coverage:
	@go test -coverprofile=coverage.out -race $$(go list ./... | grep -v /vendor/)

test-coverage-report-func:
	@go tool cover -func=coverage.out

# Run all verification commands.
verify: format vet test-coverage

# Build all command programs into bin/<arch-sys>/. If no arch and sys is passed
# to make then the output will be saved in bin/native/ and native arch and sys
# is assumed.
# example: make build arch=amd64 sys=linux
ifeq ($(CI_PROJECT_DIR),)
CI_PROJECT_DIR := .
endif
build:
ifeq ($(or $(arch),$(sys)),)
	@mkdir -p ${CI_PROJECT_DIR}/bin/native/
	@go build -race -ldflags "-extldflags '-static'" -o ${CI_PROJECT_DIR}/bin/native/ $$(go list ./cmd/...)
else
	@mkdir -p ${CI_PROJECT_DIR}/bin/$(arch)-$(sys)/
	@GOARCH=${arch} GOOS=${sys} go build -race -ldflags "-extldflags '-static'" -o ${CI_PROJECT_DIR}/bin/$(arch)-$(sys)/ $$(go list ./cmd/...)
endif

.PHONY: clean
clean:
	@go clean $$(go list ./...)
	@rm -rf ./bin/*

